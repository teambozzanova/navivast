let listItems, filterAreaBtns, filterTypeBtns, activeAreaFilter = [], activeTypeFilter = []; 
var select = null;
var markers = window.markers || [];

(function( $ ) {

    function update_filter() {
        activeAreaFilter = $(select).val() || [];
        activeTypeFilter = [];

        // filterAreaBtns.forEach( function( input ) {
        //     if ( input.selected ) {
        //         activeAreaFilter.push( input.value );
        //     }
        // } );

        filterTypeBtns.forEach( function( input ) {
            if ( input.checked ) {
                activeTypeFilter.push( input.value );
            }
        } );
            
        // console.log( 'area: ', activeAreaFilter );
        // console.log( 'type: ', activeTypeFilter );

        update_posts();
    }

    function update_posts() {
        // let markerItems = document.querySelectorAll( '.poi-marker button' );
        // let new_bounds = new google.maps.LatLngBounds();;

        // console.log( activeAreaFilter.length > 0 )
        if ( (activeAreaFilter.length > 0) || (activeTypeFilter.length > 0) ) {

            markers.forEach( function( marker ) {
                marker.setVisible( false );
            });

            listItems.forEach( function( post ) {
                let showArea = null;
                let showType = null;
                // let show = true;

                post.hidden = true;

                let id = post.dataset.id;
                let area = post.dataset.locality;
                let types = post.dataset.types.split(' ');
                // console.log( types );
                
                if ( activeAreaFilter.length > 0 ) {
                    if ( activeAreaFilter.includes( area ) ) {
                        showArea = true;
                    } else {
                        showArea = false;
                    }
                } else {
                    showArea = true;
                }

                if ( ( activeTypeFilter.length > 0 ) ) {
                    current = null;
                    types.forEach( (type) => {
                        // console.log( type );
                        if ( activeTypeFilter.includes( type ) ) {
                            current = true;
                            showType = true;
                        } else {
                            if ( ! current ) {
                                showType = false;
                            }
                        }
                    });
                } else {
                    showType = true;
                }

                // types.forEach( (type) => {
                //     if ( activeTypeFilter.includes( type ) ) {
                //         showType = true;
                //     } 
                // } );

                // console.log( 'area: ', showArea );
                // console.log( 'type: ', showType );  
                // SHOW STUFF
                if ( showArea && showType ) {
                // if ( ! activeAreaFilter.includes( area ) && ! activeTypeFilter.includes( type ) ) {
                    post.hidden = false;
                    
                    markers.find( ( marker ) => {
                        // console.log( 'post', post );
                        if ( post.dataset.id == marker.id ) {
                            marker.setVisible( true );

                        }
                    } );
                    
                    // let marker = document.querySelector( '.poi-marker-' + id );
                    // marker.hidden = false;
                    
                    // new_marker = markers.filter( function( id ) {
                    //     return this.id = id;
                    // } );
                } 
                    
            } );

            
        } else {
            listItems.forEach( function( post ) {
                post.hidden = false;
            });

            markers.forEach( function( marker ) {
                marker.setVisible( true );
            });
        }
    }

    function open_close_filter_dialog() {
        let clicker = this;
        let target = document.querySelector( '[data-filter-dialog]' );
        let body = document.querySelector('body');
        
        if ( target.classList.contains('open')) {
            clicker.setAttribute('aria-expanded', 'false');   
            target.hidden = true;
            target.classList.remove('open');
            body.classList.remove('no-scroll');
        } else {
            clicker.setAttribute('aria-expanded', 'true');
            target.hidden = false;
            target.classList.add('open');
            body.classList.add('no-scroll');
        }
    }

    function open_clear_filter_dialog() {
        
        $(select).val(null).trigger('change');

        filterTypeBtns.forEach( function( item ) {
            item.checked = false;
        } );

        update_filter();
    }

    $(window).on("elementor/frontend/init", function () {

        elementorFrontend.hooks.addAction( "frontend/element_ready/map-filter.default", function( $scope, $) {

            listItems = document.querySelectorAll( '.poi-item' ); 
            // filterAreaBtns = document.querySelectorAll( '.filter-area' );
            filterTypeBtns = document.querySelectorAll( '.filter-type' );
            
            // filterAreaBtns.forEach( function( item ) {
            //     item.addEventListener( 'click', update_filter );
            // } );
            filterTypeBtns.forEach( function( item ) {
                item.addEventListener( 'click', update_filter );
            } );

            setTimeout( function() {
                update_filter();
                // console.log( item.checked );
            }, 250 );

            let openFilterBtn = document.querySelector( '[data-open-filter]' );
            let closeFilterBtn = document.querySelector( '[data-close-filter]' );
            let clearFilterBtn = document.querySelector( '[data-clear-filter]' );

            openFilterBtn.addEventListener( 'click', open_close_filter_dialog );
            closeFilterBtn.addEventListener( 'click', open_close_filter_dialog );
            clearFilterBtn.addEventListener( 'click', open_clear_filter_dialog );

            select = $('.select2').select2();

            $(select).on( 'change', function( e ) {

                // activeAreaFilter = $(select).val();
                update_filter();
            })
        } );

    });
} )(jQuery);