var map, canvas, bounds = null, infobox, center, markers = [];
(function( $ ) {

    $(window).on("elementor/frontend/init", function () {
        
        elementorFrontend.hooks.addAction(
            "frontend/element_ready/nova-map.default",
            function ($scope, $) {

                let close_btn = document.getElementById('close');
                close_btn.addEventListener( 'click', close_infobox );

                canvas = document.getElementById( 'map' );

                // if ( canvas.length == 0 ) return;

                infobox = document.getElementById( 'infobox' );

                bounds = new google.maps.LatLngBounds();

                map = new google.maps.Map( canvas, {
                    center: { lat: 58.8317269, lng: 12.641363 },
                    zoom: 10,
                } );

                pois.forEach( poi => setMarkers( poi ) );
        
                if ( markers.length > 1 ) {
                    map.fitBounds(bounds);
                    center = map.getCenter();
                }

                if ( markers.length == 1 ) {
                    map.setCenter( markers[0].getPosition() );
                    map.setZoom( 14 );
                }

                center = map.getCenter();

            }
        );

        function setMarkers( poi ) {
            const icon_default = {
                url: poi.icon_default,
                size: new google.maps.Size(40, 40),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(20, 40),
                scaledSize: new google.maps.Size(40, 40)
            };

            const icon_active = {
                url: poi.icon_active,
                size: new google.maps.Size(40, 40),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(20, 40),
                scaledSize: new google.maps.Size(40, 40)
            };
        
            const latlng = { lat: poi.lat, lng: poi.lng };

            let image_content = '';
            if ( false !== poi.image ) {
                let image_content = '<div class="poi-image"><img src="' + poi.image + '" alt="" height="150" width="150"></div>';
            } 


            const infoContent =
                '<div class="poi-body">' +
                image_content +
                '<div class="poi-content">' +
                '<div class="poi-name">' + poi.name + '</div>' +
                // '<div class="poi-address">' + poi.address + '</div>' +
                '<div class="poi-link"><a href="' + poi.link + '">Mer information</a></div>' +
                '</div></div>';


            const marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: icon_default,
                icon_default: icon_default,
                icon_active: icon_active,
                info: infoContent,
                title: poi.name,
                id: poi.id,
                type: poi.type,
                locality: poi.locality
            });

            marker.addListener( "click", () => {
                center = map.getCenter();
                // infowindow.open(map, marker);
                open_infobox( marker );
            });
            
            bounds.extend( marker.getPosition() );
            
            markers.push( marker );
        
        }

        function open_infobox( marker ) {
            close_infobox();

            // center map
            map.panTo( marker.getPosition() );
            map.panBy( 0, -150 );

            // set active marker
            marker.setIcon( marker.icon_active );

            // fill info
            const info = document.createElement("div");
            info.classList.add('info');
            info.innerHTML = marker.info;
            infobox.appendChild(info);
            infobox.classList.add('open');
        }

        function close_infobox() {
            
            infobox.classList.remove('open');

            markers.forEach( marker => {
                marker.setIcon( marker.icon_default );
            } );

            const info = infobox.querySelector('.info');
            if ( info ) {
                infobox.removeChild(info);
            }

            map.panTo( center );
        }

        

    } );

} )(jQuery);