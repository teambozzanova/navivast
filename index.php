<?php
/**
 * Index
 *
 * @package Nova
 */

 if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php $viewport_content = apply_filters( 'hello_elementor_viewport_content', 'width=device-width, initial-scale=1' ); ?>
	<meta name="viewport" content="<?php echo esc_attr( $viewport_content ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
$is_elementor_theme_exist = function_exists( 'elementor_theme_do_location' );
//hello_elementor_body_open();

// HEADER
if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'header' ) ) {
	// if ( did_action( 'elementor/loaded' ) && hello_header_footer_experiment_active() ) {
	// 	get_template_part( 'parts/dynamic-header' );
	// } else {
		get_template_part( 'parts/header' );
	// }
}

// MAIN
if ( is_singular() ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single' ) ) {
		get_template_part( 'parts/single' );
	}
} elseif ( is_archive() || is_home() ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'archive' ) ) {
		get_template_part( 'parts/archive' );
	}
} elseif ( is_search() ) {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'archive' ) ) {
		get_template_part( 'parts/search' );
	}
} else {
	if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'single' ) ) {
		get_template_part( 'parts/404' );
	}
}

// FOOTER
if ( ! $is_elementor_theme_exist || ! elementor_theme_do_location( 'footer' ) ) {
	// if ( did_action( 'elementor/loaded' ) && hello_header_footer_experiment_active() ) {
	// 	get_template_part( 'parts/dynamic-footer' );
	// } else {
		get_template_part( 'parts/footer' );
	// }
}

wp_footer(); ?>
</body>
</html>
