<?php
/**
 * @package   Novastyle 2020
 * @author    Jon Täng <jon@bozzanova.se>
 * @link      http://www.bozzanova.se
 * @copyright 2020 Bozzanova AB
 */

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * News Post Type Setup
 */
class News {

  private static $instance = null;

  public $post_type = null;
  public $post_type_rewrite = null;
  
  public $singular = null;
  public $plural = null;
  
  public $category = null;
  public $singular_cat = null;
  public $plural_cat = null;

  public $tag = null;
  public $singular_tag = null;
  public $plural_tag = null;
  
  public $archive_page = null;
  public $archive_page_slug = null;

	
	/**
	 * Instance function
	 *
	 * @return Theme
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

  /**
   * Class Construct
   *
   * @return void
   */
  public function __construct() {
    add_action( 'init', function () {
      $this->post_type = 'news';
      $this->post_type_rewrite = _x( 'news', 'post type rewrite', 'nova' );
      $this->singular = _x( 'News', 'Post Type Singular Name', 'nova' );
      $this->plural = _x( 'News', 'Post Type Plural Name', 'nova' );

      $this->category = 'news_category';
      $this->singular_cat = _x( 'News Category', 'Category Singular Name', 'nova' );
      $this->plural_cat = _x( 'News Categories', 'Category Plural Name', 'nova' );
      
      $this->tag = 'news_tag';
      $this->singular_tag = _x( 'News Tag', 'Tag Singular Name', 'nova' );
      $this->plural_tag = _x( 'News Tags', 'Tag Plural Name', 'nova' );
      
      $this->archive_page = false;
      $this->archive_page_slug = _x('news-archive', 'news archive slug', 'nova');
    }, 5 );
    
    add_action( 'init', [ $this, 'set_archive_page' ], 10 );

    // Media Categories & Tags
    add_action( 'init', [ $this, 'create_post_type' ], 15 );
    add_action( 'init', [ $this, 'create_taxonomy_category' ], 15 );
    //add_action( 'init', [ $this, 'create_taxonomy_tag' ], 15 );
    add_action( 'restrict_manage_posts', [ $this, 'filter_taxonomies' ] );
    add_action( 'save_post', [ $this, 'set_default_category'], 99 );


    // Set custom  archive page
    // add_action( 'init', [ $this, 'add_rewrite_rule' ], 20 );
    // add_action( 'pre_get_posts', [ $this, 'alter_archive_page' ] );
    // add_filter( 'post_type_archive_link', [$this, 'post_type_archive_link'], 10, 2 );

    
    
  }

  /**
   * Set Custom Archive Page
   */
  public function set_archive_page() {
    if ( !function_exists( 'get_field' ) ) {
      return;
    }

    if ( $archive_page = get_field( 'news_archive_page', 'option' ) ) {
      add_theme_support( 'custom_archive_' . $this->post_type );
      $this->archive_page = $archive_page;
    }
  }

  /**
   * Create Post type
   */
  public function create_post_type() {
  
    $labels = array(
      'name'                  => $this->plural,
      'singular_name'         => $this->singular,
      'menu_name'             => $this->plural,
      'name_admin_bar'        => $this->plural,
      'archives'              => __( 'Archives', 'nova' ),
      'attributes'            => __( 'Attributes', 'nova' ),
      'parent_item_colon'     => __( 'Parent item:', 'nova' ),
      'all_items'             => __( 'All item', 'nova' ),
      'add_new_item'          => __( 'Add New item', 'nova' ),
      'add_new'               => __( 'Add New', 'nova' ),
      'new_item'              => __( 'New item', 'nova' ),
      'edit_item'             => __( 'Edit item', 'nova' ),
      'update_item'           => __( 'Update item', 'nova' ),
      'view_item'             => __( 'View item', 'nova' ),
      'view_items'            => __( 'View items', 'nova' ),
      'search_items'          => __( 'Search items', 'nova' ),
      'not_found'             => __( 'Not found', 'nova' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'nova' ),
      'featured_image'        => __( 'Featured Image', 'nova' ),
      'set_featured_image'    => __( 'Set featured image', 'nova' ),
      'remove_featured_image' => __( 'Remove featured image', 'nova' ),
      'use_featured_image'    => __( 'Use as featured image', 'nova' ),
      'insert_into_item'      => __( 'Insert into item', 'nova' ),
      'uploaded_to_this_item' => __( 'Uploaded to this item', 'nova' ),
      'items_list'            => __( 'Items list', 'nova' ),
      'items_list_navigation' => __( 'Itemes list navigation', 'nova' ),
      'filter_items_list'     => __( 'Filter items list', 'nova' ),
    );

    $rewrite = array(
      // 'slug'                  => urldecode( get_page_uri( $this->archive_page ) ) ?: $this->archive_page_slug,
      'slug'                  => $this->post_type_rewrite,
      'with_front'            => true,
      // 'pages'                 => false,
      // 'feeds'                 => true,
    );

    $args = array(
      'label'                 => $this->singular,
      'labels'                => $labels,
      'description'           => __( 'Post Type for Visitor Newss', 'nova' ),
      'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'author' ),
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'menu_icon'             => 'dashicons-media-document',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'hierarchical'          => false,
      'can_export'            => true,
      'has_archive'           => true,
      'taxonomies'            => array( $this->category, $this->tag ),
      'rewrite'               => $rewrite,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'query_var'             => true,
      'show_in_rest'          => true,
      // 'rest_base'             => '',
      'show_in_graphql'       => false,
      'delete_with_user'      => false,
      'capability_type'       => 'page',
      'map_meta_cap'          => true
    );
/*
    if ( current_theme_supports( 'custom_archive_' . $this->post_type ) ) {
      $args['has_archive'] = ( $this->archive_page && get_post( $this->archive_page ) ) ? urldecode( get_page_uri( $this->archive_page ) ) : $this->archive_page_slug;
    } else {
      $args['has_archive'] = true;
    }
*/
    register_post_type( $this->post_type, $args );
  
  }

  /**
   * Create Category Taxonomy
   */
  public function create_taxonomy_category() {
      
    $labels = [
      'name' => $this->plural_cat,
      'singular_name' => $this->singular_cat,
      'menu_name' => $this->plural_cat,
      'all_items' => __( 'All Categories', 'nova' ),
      'edit_item' => __( 'Edit Category', 'nova' ),
      'view_item' => __( 'View Category', 'nova' ),
      'update_item' => __( 'Update Category name', 'nova' ),
      'add_new_item' => __( 'Add new Category', 'nova' ),
      'new_item_name' => __( 'New Category name', 'nova' ),
      'parent_item' => __( 'Parent Category', 'nova' ),
      'parent_item_colon' => __( 'Parent Category:', 'nova' ),
      'search_items' => __( 'Search Categories', 'nova' ),
      'popular_items' => __( 'Popular Categories', 'nova' ),
      'separate_items_with_commas' => __( 'Separate Categories with commas', 'nova' ),
      'add_or_remove_items' => __( 'Add or remove Categories', 'nova' ),
      'choose_from_most_used' => __( 'Choose from the most used Categories', 'nova' ),
      'not_found' => __( 'No Categories found', 'nova' ),
      'no_terms' => __( 'No Categories', 'nova' ),
      'items_list_navigation' => __( 'Categories list navigation', 'nova' ),
      'items_list' => __( 'Categories list', 'nova' ),
    ];
  
    $args = [
      'label' => $this->plural_cat,
      'labels' => $labels,
      'public' => true,
      //'publicly_queryable' => true,
      'hierarchical' => true,
      // 'update_count_callback' => '_update_generic_term_count',
      //'show_ui' => true,
      //'show_in_menu' => true,
      'show_in_nav_menus' => false,
      //'query_var' => sanitize_key( 'news_category' ),
      //'rewrite' => false,
      'show_admin_column' => true,
      'show_in_rest' => true,
      //'rest_base' => 'news_category',
      //'rest_controller_class' => 'WP_REST_Terms_Controller',
      'show_in_quick_edit' => true,
      'admin_filter' => true,
      'sort' => true,
    ];

    register_taxonomy( $this->category, $this->post_type, $args );
  
  }

  /**
   * Create Tag Taxonomy
   */
  public function create_taxonomy_tag() {

    $labels = [
      'name' => $this->plural_tag,
      'singular_name' => $this->singular_tag,
      'menu_name' => $this->plural_tag,
      'all_items' => __( 'All Tags', 'nova' ),
      'edit_item' => __( 'Edit Tag', 'nova' ),
      'view_item' => __( 'View Tag', 'nova' ),
      'update_item' => __( 'Update Tag name', 'nova' ),
      'add_new_item' => __( 'Add new Tag', 'nova' ),
      'new_item_name' => __( 'New Tag name', 'nova' ),
      'parent_item' => __( 'Parent Tag', 'nova' ),
      'parent_item_colon' => __( 'Parent Tag:', 'nova' ),
      'search_items' => __( 'Search Tags', 'nova' ),
      'popular_items' => __( 'Popular Tags', 'nova' ),
      'separate_items_with_commas' => __( 'Separate Tags with commas', 'nova' ),
      'add_or_remove_items' => __( 'Add or remove Tags', 'nova' ),
      'choose_from_most_used' => __( 'Choose from the most used Tags', 'nova' ),
      'not_found' => __( 'No Tags found', 'nova' ),
      'no_terms' => __( 'No Tags', 'nova' ),
      'items_list_navigation' => __( 'Tags list navigation', 'nova' ),
      'items_list' => __( 'Tags list', 'nova' ),
    ];
  
    $args = [
      'label' => $this->plural_tag,
      'labels' => $labels,
      'public' => true,
      //'publicly_queryable' => false,
      'hierarchical' => false,
      // 'update_count_callback' => '_update_generic_term_count',
      //'show_ui' => true,
      //'show_in_menu' => true,
      'show_in_nav_menus' => false,
      //'query_var' => false,
      //'rewrite' => false,
      'show_admin_column' => true,
      'show_in_rest' => true,
      //'rest_base' => 'news_tag',
      //'rest_controller_class' => 'WP_REST_Terms_Controller',
      'show_in_quick_edit' => true,
      'sort' => true,
    ];

    register_taxonomy( $this->tag, $this->post_type, $args );
  
  }

  /**
   * Create Filter in Post List in Admin
   */
  function filter_taxonomies() {
    $scr = get_current_screen();
        
    if ( $scr->base == 'edit' && $scr->post_type == $this->post_type ) {

      $taxonomies = array( $this->category );
      
      foreach ( $taxonomies as $taxonomy ) {
        $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);

        $terms = get_terms( $taxonomy, array(
          'hide_empty' => false,
        ) );

        if ( empty( $terms ) ) 
          return;

        wp_dropdown_categories( array(
          'show_option_all' => sprintf( __( 'Show all %s', 'nova' ), $info_taxonomy->label ),
          'taxonomy'        => $taxonomy,
          'name'            => $taxonomy,
          'value_field'     => 'slug',
          'orderby'         => 'name',
          'selected'        => $selected,
          'show_count'      => true,
          'hide_empty'      => true,
        ) );
      }
    }
  }

  /**
   * Add Custom Rewrite Rule
   */
  public function add_rewrite_rule() {
    $page_obj = get_post( $this->archive_page );
    if ( $page_obj ) {
		  add_rewrite_rule( "^{$page_obj->post_name}/page/([0-9]+)/?", 'index.php?pagename=' . $page_obj->post_name . '&paged=$matches[1]', 'top' );
    }
  }
  
  /**
   * Alter Post Type Archive Page
   * 
   * @param object $query
   * 
   * @return object $query
   */
  public function alter_archive_page( $query ) {
    
    // if ( is_admin () || !$query->is_main_query() ) {
    //   return $query;
    // }

		// if ( $page_for_archive = $this->archive_page ) {
    //   return $query;
    // }

		if (isset( $query->queried_object_id ) && $query->queried_object_id == $page_for_archive ) {
			$query->set( 'post_type', 'news' );
			$query->set( 'page', '' );
			$query->set( 'pagename', '' );
			// $query->set( 'tax_query', [[
			// 	'taxonomy' => 'news_category',
			// 	'field' => 'slug',
			// 	'terms' => 'fyrbodal'
			// ]]);

			$query->is_archive = true;
			$query->is_post_type_archive = true;
			$query->is_singular = false;
			$query->is_page = false;

			add_filter( 'wpseo_title', [$this, 'set_queried_object_title'] );
			add_filter( 'wpseo_breadcrumb_links', [$this, 'set_queried_object_breadcrumb'] );
			//add_filter( 'posts_clauses', [$this, 'posts_clauses_campaign'], 10, 2 );

		}

		return $query;
  }
  
  /** 
   * Post Type Archive Link
   * 
   * @param string $link
   * @param object $post_type
   * 
   * @return string $link
   */
  public function post_type_archive_link( $link, $post_type ) {
    if ( $post_type !== $this->post_type ) 
      return $link;

		return get_permalink( $this->archive_page );
	}

  /**
   * Set default category
   * 
   * @param int $post_id
   * 
   * @return void
   */
  public function set_default_category( $post_id ) {
    $has_terms = get_the_terms( $post_id, $this->category );

    if ( $this->post_type !== get_post_type( $post_id ) ) {
      return;
    }

    if ( ! $has_terms && $default_term = get_field( 'news_default_category', 'option' )) {
      wp_set_post_terms(
        $post_id,
        [ $default_term ],
        $this->category
      );
    }
  }

}