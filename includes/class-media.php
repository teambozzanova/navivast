<?php
/**
 * @package   Novastyle 2020
 * @author    Jon Täng <jon@bozzanova.se>
 * @link      http://www.bozzanova.se
 * @copyright 2020 Bozzanova AB
 */

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Media Post Type Setup
 */
class Media {

  private static $instance = null;
	
	/**
	 * Instance function
	 *
	 * @return Theme
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

  /**
   * Class Construct
   *
   * @return void
   */
  public function __construct() {
    // Set Image Sizes
    add_action( 'after_setup_theme', [ $this, 'media_setup' ] );
    // Add Custom Image Size Names
    // add_filter( 'image_size_names_choose', [ $this, 'custom_image_sizes' ] );
    // add_filter( 'wp_calculate_image_sizes', [ $this, 'attachment_image_attributes' ], 10, 3 );
    
    // Enable Lazyload
    add_filter( 'wp_lazy_loading_enabled', '__return_true' );
    
    // Handle Uploads
    add_filter( 'wp_handle_upload_prefilter', [ $this, 'sanitize_uploaded_file_names' ] );
    add_filter( 'jpeg_quality', function( $int ){
			return 100;
		});
    // Add SVG Support
    // add_filter( 'upload_mimes', [ $this, 'mime_types' ] );
    // add_filter( 'wp_check_filetype_and_ext', [ $this, 'check_file_type'], 10, 4 );

    // Media Categories & Tags
    add_action( 'init', [ $this, 'create_taxonomies' ] );
    add_action( 'restrict_manage_posts', [ $this, 'filter_taxonomies' ] );

    // add_filter( 'the_content', [ $this, 'responsive_iframe' ] );
  }

  public function media_setup() {
    
    update_option( 'thumbnail_size_w', 200 );
    update_option( 'thumbnail_size_h', 200 );
    update_option( 'thumbnail_crop', 1 );
    update_option( 'medium_size_w', 768 );
    update_option( 'medium_size_h', 768 );
    update_option( 'medium_crop', 0 );
    update_option( 'medium_large_size_w', 992 );
    update_option( 'medium_large_size_h', 992 );
    update_option( 'medium_large_crop', 0 );
    update_option( 'large_size_w', 1200 );
    update_option( 'large_size_h', 1200 );
    update_option( 'large_crop', 0 );
    
    // Set post thumbnail size.
    //set_post_thumbnail_size( 1200, 9999 );

    // Add custom image size used in Cover Template.
    // add_image_size( 'square', 768, 768, true );           // 1:1
    // add_image_size( 'square-480', 480, 480, true );       // 1:1
    // add_image_size( 'portrait', 768, 1280, true );        // 3:5
    // add_image_size( 'portrait-480', 460, 800, true );     // 3:5
    // add_image_size( 'landscape', 960, 768, true );        // 5:4
    // add_image_size( 'landscape-768', 768, 615, true );    // 5:4
    // add_image_size( 'landscape-480', 480, 384, true );    // 5:4
    // add_image_size( 'hero', 2560, 1280, true );           // 2:1
    // add_image_size( 'hero-1920', 1920, 960, true );       // 2:1
    // add_image_size( 'hero-768', 768, 384, true );         // 2:1
    // add_image_size( 'hero-480', 480, 480, true );         // 1:1
    // add_image_size( 'square', 960, 960, true );           // 1:1
    // add_image_size( 'square-768', 768, 768, true );       // 1:1
    // add_image_size( 'square-480', 480, 480, true );       // 1:1

    // add_image_size( 'logo', 400, 100, false );            // 4:1
    // add_image_size( 'marker', 40, 40, false );            // 1:1
    /*
    // Custom logo.
    $logo_width  = 400;
    $logo_height = 100;

    // If the retina setting is active, double the recommended width and height.
    if ( get_theme_mod( 'retina_logo', false ) ) {
      $logo_width  = floor( $logo_width * 2 );
      $logo_height = floor( $logo_height * 2 );
    }

    // Custom logo
    add_theme_support(
      'custom-logo',
      [
        'height'      => $logo_height,
        'width'       => $logo_width,
        'flex-height' => true,
        'flex-width'  => true,
      ]
    );
    */

  }

  // function attachment_image_attributes( $sizes, $size ) {
  //   $width = $size[0];

  //   echo '<pre>'; print_r( $size ); echo '</pre>';
	// echo '<pre>'; print_r( $sizes ); echo '</pre>';

  //   if (  ) {
  //       $sizes['sizes'] = '(min-width: 1200px) 480px, (min-width: 960px) 768px, (min-width: 768px) 768px, 480px';
  //   }

  //   return $sizes;
  // }
    
    

  /**
	 * Mime Types
	 */
	public function mime_types( $mimes ) {
		$mimes['eps'] = 'application/postscript';
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';

		return $mimes;
  }
  
  /**
   * Check file type
   */
  function check_file_type($data, $file, $filename, $mimes) {
    global $wp_version;

    if ( $wp_version !== '4.7.1' ) {
       return $data;
    }
  
    $filetype = wp_check_filetype( $filename, $mimes );
  
    return [
        'ext'             => $filetype['ext'],
        'type'            => $filetype['type'],
        'proper_filename' => $data['proper_filename']
    ];
  
  }

	/**
	 * Add Image Sizes to Media Selector
	 *
	 * @param [type] $sizes
	 * @return void
	 */
	function custom_image_sizes( $sizes ) {
		$custom_sizes = [
			// 'square' => __('Square', 'nova'),
			// 'portrait' => __('Portrait', 'nova'),
			'landscape' => __('Landscape', 'nova'),
			'landscape-768' => __('Landscape 768px', 'nova'),
			'landscape-480' => __('Landscape 480px', 'nova'),
			'hero' => __('Hero', 'nova'),
			'hero-1920' => __('Hero 1920px', 'nova'),
			'hero-768' => __('Hero 768px', 'nova'),
			'hero-480' => __('Hero 480px', 'nova'),
      'logo' => __('Logotype', 'nova'),
      'marker' => __('Map Marker', 'nova'),
		];

		return array_merge( $sizes, $custom_sizes );
	}

	/** 
	 * Sinitize file names in upload
	 */
	public function sanitize_uploaded_file_names( $file ) {
		$file['name'] = sanitize_file_name( $file['name'] );
    $file['name'] = str_replace( ['å','ä','ö','Å','Ä','Ö'], ['a','a','o','A','A','O'], $file['name'] );
		$file['name'] = preg_replace( "/[^a-zA-Z0-9\_\-\.]/", "", $file['name'] );
		$file['name'] = strtolower( $file['name'] );

		add_filter( 'sanitize_file_name', 'remove_accents' );

		return $file;
  }
  
	/**
	 * Responsive Iframe
	 *
	 * @param [type] $content
	 * @return void
	 */
	public function responsive_iframe( $content ) {
		$content = str_replace(array("<iframe", "</iframe>"), array('<div class="responsive-iframe"><iframe', "</iframe></div>"), $content);
	
		return $content;
	}

	function fixed_img_caption_shortcode( $width ) {
		return 0;
  }
  

  public function create_taxonomies() {
      
    $cat_labels = [
      'name' => __( 'Media Categories', 'nova' ),
      'singular_name' => __( 'Media Category', 'nova' ),
      'menu_name' => __( 'Media Categories', 'nova' ),
      'all_items' => __( 'All Categories', 'nova' ),
      'edit_item' => __( 'Edit Category', 'nova' ),
      'view_item' => __( 'View Category', 'nova' ),
      'update_item' => __( 'Update Category name', 'nova' ),
      'add_new_item' => __( 'Add new Category', 'nova' ),
      'new_item_name' => __( 'New Category name', 'nova' ),
      'parent_item' => __( 'Parent Category', 'nova' ),
      'parent_item_colon' => __( 'Parent Category:', 'nova' ),
      'search_items' => __( 'Search Categories', 'nova' ),
      'popular_items' => __( 'Popular Categories', 'nova' ),
      'separate_items_with_commas' => __( 'Separate Categories with commas', 'nova' ),
      'add_or_remove_items' => __( 'Add or remove Categories', 'nova' ),
      'choose_from_most_used' => __( 'Choose from the most used Categories', 'nova' ),
      'not_found' => __( 'No Categories found', 'nova' ),
      'no_terms' => __( 'No Categories', 'nova' ),
      'items_list_navigation' => __( 'Categories list navigation', 'nova' ),
      'items_list' => __( 'Categories list', 'nova' ),
    ];
  
    $cat_args = [
      'label' => __( 'Media Categories', 'nova' ),
      'labels' => $cat_labels,
      'public' => true,
      //'publicly_queryable' => true,
      'hierarchical' => true,
      'update_count_callback' => '_update_generic_term_count',
      //'show_ui' => true,
      //'show_in_menu' => true,
      'show_in_nav_menus' => false,
      'query_var' => sanitize_key( 'media_category' ),
      //'rewrite' => false,
      'show_admin_column' => true,
      //'show_in_rest' => false,
      //'rest_base' => 'media_category',
      //'rest_controller_class' => 'WP_REST_Terms_Controller',
      //'show_in_quick_edit' => true,
      'admin_filter' => true,
      'sort' => true,
    ];
    register_taxonomy( 'media_category', 'attachment', $cat_args );

    $tag_labels = [
      'name' => __( 'Media Tags', 'nova' ),
      'singular_name' => __( 'Media Tag', 'nova' ),
      'menu_name' => __( 'Media Tags', 'nova' ),
      'all_items' => __( 'All Tags', 'nova' ),
      'edit_item' => __( 'Edit Tag', 'nova' ),
      'view_item' => __( 'View Tag', 'nova' ),
      'update_item' => __( 'Update Tag name', 'nova' ),
      'add_new_item' => __( 'Add new Tag', 'nova' ),
      'new_item_name' => __( 'New Tag name', 'nova' ),
      'parent_item' => __( 'Parent Tag', 'nova' ),
      'parent_item_colon' => __( 'Parent Tag:', 'nova' ),
      'search_items' => __( 'Search Tags', 'nova' ),
      'popular_items' => __( 'Popular Tags', 'nova' ),
      'separate_items_with_commas' => __( 'Separate Tags with commas', 'nova' ),
      'add_or_remove_items' => __( 'Add or remove Tags', 'nova' ),
      'choose_from_most_used' => __( 'Choose from the most used Tags', 'nova' ),
      'not_found' => __( 'No Tags found', 'nova' ),
      'no_terms' => __( 'No Tags', 'nova' ),
      'items_list_navigation' => __( 'Tags list navigation', 'nova' ),
      'items_list' => __( 'Tags list', 'nova' ),
    ];
  
    $tag_args = [
      'label' => __( 'Media Tags', 'nova' ),
      'labels' => $tag_labels,
      'public' => true,
      //'publicly_queryable' => false,
      'hierarchical' => false,
      'update_count_callback' => '_update_generic_term_count',
      //'show_ui' => true,
      //'show_in_menu' => true,
      'show_in_nav_menus' => false,
      //'query_var' => false,
      //'rewrite' => false,
      'show_admin_column' => true,
      //'show_in_rest' => false,
      //'rest_base' => 'media_tag',
      //'rest_controller_class' => 'WP_REST_Terms_Controller',
      'show_in_quick_edit' => true,
      'sort' => true,
    ];
    register_taxonomy( 'media_tag', [ 'attachment' ], $tag_args );
  
  }

  function filter_taxonomies() {
    $scr = get_current_screen();
        
    if ( $scr->base !== 'upload' ) 
      return;
  
    $taxonomies = array( 'media_category', 'media_tag' );
    
    foreach ( $taxonomies as $taxonomy ) {
      $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
      $info_taxonomy = get_taxonomy($taxonomy);

      $terms = get_terms( $taxonomy, array(
        'hide_empty' => false,
      ) );

      if ( empty( $terms ) ) 
        return;

      wp_dropdown_categories( array(
        'show_option_all' => sprintf( __( 'Show all %s', 'nova' ), $info_taxonomy->label ),
        'taxonomy'        => $taxonomy,
        'name'            => $taxonomy,
        'value_field'     => 'slug',
        'orderby'         => 'name',
        'selected'        => $selected,
        'show_count'      => true,
        'hide_empty'      => true,
      ) );
    }
  }
}