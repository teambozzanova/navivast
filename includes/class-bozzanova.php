<?php
/**
 * class-bozzanova.php
 * A class for managing bozzanova branding
 * 
 * @package		NovaTheme
 * @author		Jon Täng <jon@bozzanova.se>
 * @link	  	https://www.bozzanova.se/
 * @copyright	Bozzanova AB
 */

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Bozzanova {
	
	public $site = '';
	public $developer = '';
	public $website =  '';
	public $facebook = '<a href="https://www.facebook.com/bozzanova.se/" target="_blank">Facebook</a>';
	public $linkedin = '<a href="http://www.linkedin.com/company/bozzanova-ab" target="_blank">LinkedIn</a>';
	public $twitter = '<a href="https://twitter.com/bozzanova" target="_blank">Twitter</a>';
	public $instagram = '<a href="https://www.instagram.com/bozzanova.se/" target="_blank">Instagram</a>';
	
	private static $instance = null;
	
	/**
	* Instance function
	*
	* @return Theme
	*/
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	function __construct() {
		add_action( 'after_setup_theme', [ $this, 'set_site' ] );
		//$this->set_site();
		// Branding
		add_filter( 'admin_footer_text', [ $this, 'admin_footer' ]);
		add_action( 'wp_dashboard_setup', [ $this, 'dashboard_widgets' ] );
		
		// Support Page
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );

		add_action( 'admin_bar_menu', [ $this, 'customize_toolbar' ], 999 );
	}
	
	public function set_site() {
		$site = get_site_url(); 
		$site = str_replace('https://', '', $site);
		$site = str_replace('http://', '', $site);
		
		$this->site = $site;
		$this->developer = sprintf( '<a href="https://www.bozzanova.se/?utm_source=customer_site&utm_medium=website&utm_campaign=support&utm_content=%s" target="_blank">%s</a>', $site, __( 'Bozzanova Agency', 'nova' ) );
		$this->website = sprintf( '<a href="https://www.bozzanova.se/?utm_source=customer_site&utm_medium=website&utm_campaign=support&utm_content=%s" target="_blank">%s</a>', $site, __( 'Website', 'nova' ) );
	}
	
	//--------------------------------------------------------------------------------
	//	Bozzanova THEME Brading	
	//--------------------------------------------------------------------------------
	
	/*
	* Custom Backend Footer
	*/
	public function admin_footer() {
		printf( 
			'<span id="footer-thank-you">%s</span>',
			sprintf( '%s %s',
			__('Developed by', 'nova'),
			$this->developer
			)
		);
	}

	/**
	 * Add link in WP Admin Toolbar
	 * 
	 * @param array $wp_admin_bar
	 * @return array $wp_admin_bar
	 */
	public function customize_toolbar( $wp_admin_bar ) {
		// echo '<pre>'; print_r( $wp_admin_bar ); echo '</pre>';
		$wp_admin_bar->add_node( [
			'id'		=> 'bozzanova-support',
			'title'		=> sprintf('<span class="ab-icon"></span><span class="ab-label">%s</span>', __( 'Bozzanova Support',  'nova' ) ),
			'href'		=> admin_url( 'index.php?page=bozzanova-support' ),
			'parent'    => 'top-secondary'
     ] );
	}
	
	/**
	* Create the function to output the contents of our Dashboard Widget.
	*/
	public function dashboard_widget() {
		// Display whatever it is you want to show.
		$current_user = wp_get_current_user();
		printf( '<div><strong>%s %s</strong><br>%s</div>', 
		__('Hello', 'nova'), 
		($current_user->firsname_name !== '') ? $current_user->firsname_name: $current_user->display_name, 
		__( 'Thanks for using a theme from Bozzanova Agency', 'nova' ) 
	);
	
	echo '<div style="border-bottom: 1px solid #eee;margin: 0 -12px;padding: 8px 12px 4px;">';
	printf( '<h2>%s</h2>', __('Need support?', 'nova' ) );
	printf( '<div>%s<br>%s: %s<br>%s: %s</div>', 
	__('You are welcome to contact us if you need any kind of help.', 'nova'),
	__('Call', 'nova' ),
	'0521 - 66 100',
	__('Email', 'nova' ),
	'<a href="mailto:support@bozzanova.se">support@bozzanova.se</a>'
);
printf('<div style="margin: .5rem 0;">%s %s %s</div>', 
__('Visit our', 'nova' ),
$this->website,
__('for more information about our services.', 'nova' )
);

printf('<div style="margin: .5rem 0;">%s: %s, %s, %s, %s</div>',
__('Follow Bozzanova on', 'nova'),
$this->facebook,
$this->linkedin,
$this->twitter,
$this->instagram
);

printf('<div style="margin: .5rem 0;">%s</div>',
sprintf('<a href="index.php?page=bozzanova-support">%s</a>', __('For more support information', 'nova') )
);
echo '</div>';
}

/**
* Add a widget to the dashboard.
*
* This function is hooked into the 'wp_dashboard_setup' action below.
*/
public function dashboard_widgets() {
	wp_add_dashboard_widget(
		'bozzanova_dashboard_widget', // Widget slug.
		__('Bozzanova Agency', 'nova' ), // Title.
		[ $this, 'dashboard_widget' ] // Display function.
	);	
}

//--------------------------------------------------------------------------------
//	Bozzanova THEME Support Page	
//--------------------------------------------------------------------------------
/** 
* Add Support Page
*/
function admin_menu() {
	add_dashboard_page(
		__('Bozzanova Support', 'nova' ),
		__('Support', 'nova' ),
		'read',
		'bozzanova-support',
		[ $this, 'render_page' ]
	);
}

/** 
* Render Support Page
*/
function render_page() {
	$site = get_site_url(); 
	$site = str_replace('https://', '', $site);
	$site = str_replace('http://', '', $site);
	//get_template_part('parts/admin/support');
	?>
	<style>
	.novastyle {
		font-size: 16px;
	}

	
	.novastyle .inside {
		padding: 1rem 2rem;
	}
	.novastyle h1 {
		display: flex;
		align-items: center;
	}

	.novastyle .icon {
		position:relative;
		margin-right: 20px;
		width: 40px;
		height: 40px;
		display: inline-block;		
	}

	.novastyle .icon svg {
		width: 100%;
		height: auto;
		
	}

	.novastyle h1 {
		font-size: 2.5rem;
		margin: 1rem 0;
	}
	.novastyle h2,
	.postbox .inside h2 {
		font-size: 1.8rem;
		margin-top: 1rem;
		margin-bottom: .75rem;
		padding: 0;
	}
	.novastyle h3,
	.novastyle h4 {
		font-weight: 400;
	}
	
	.novastyle h3 {
		font-size: 1.3rem;
		margin-top: 1.5rem;
		margin-bottom: .25rem;
	}
	.novastyle h4 {
		font-size: 1.1rem;
		margin-top: 1.25rem;
		margin-bottom: .125rem;
	}
	
	.novastyle p {
		margin-top: 0;
		font-size: 1rem;
	}
	
	.novastyle .row { 
		
		margin-left: -15px;
		margin-right: -15px;
		
		padding-top: 10px;
		
		display: flex;
		flex-direction: row;
		flex-flow: nowrap;
	}
	
	.novastyle .col {
		padding-left: 15px;
		padding-right: 15px;
		
		width: 50%;
	}
	
	.novastyle .left {
		order: 2;
	}
	.novastyle .right {
		order: 1;
	}
	
	@media (max-width: 1024px) {
		.novastyle .row { 
			flex-direction: column;
			flex-flow: wrap;
		}
		.novastyle .col {
			width: 100%;            
		}
		
		.novastyle .left {
			order: 1;
		}
		.novastyle .right {
			order: 2;
		}
	}
	</style>
	<div class="wrap novastyle">
	<h1><span class="icon"><svg aria-hidden="true" focusable="false" data-icon="bozzanova" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.46 283.46"><rect fill="currentColor" y="261.66" width="239.17" height="21.8"/><rect fill="currentColor" y="218.05" width="283.46" height="21.8"/><rect fill="currentColor" y="174.44" width="283.46" height="21.8"/><rect fill="currentColor" y="130.83" width="256.89" height="21.8"/><rect fill="currentColor" y="87.22" width="283.46" height="21.8"/><rect fill="currentColor" y="43.61" width="283.46" height="21.8"/><rect fill="currentColor" width="239.17" height="21.8"/></svg></span>Bozzanova Support</h1>
	
	<div class="poststuff">
	
	<div class="row">
	
	<div class="col left">
	
	<div class="postbox">
	<div class="inside">
	<h2>Behöver ni support?</h2>
	<p>Ni är välkomna att kontakta oss om ni behöver hjälp med hemsidan, e-post eller på något annat vis.</p>
	<p>Ring oss på: <strong>0521 - 66 100</strong> eller skicka e-post till <a href="mailto:support@bozzanova.se?subject=Vi behöver hjälp med <?php echo $site; ?>"><strong>support@bozzanova.se</strong></a></p>       
	<p><strong>OBS!</strong><br> Innan ni ringer eller mejlar om problem med hemsidan är det bra att ta skärmbild på felet/problemet och noterar vilken länk på hemsidan det gäller. Då blir det lättare för oss att felsöka eller svara på de frågor ni har.</p>
	
	<h3>Support-verktyg</h3>
	<p>Ladda ner <a href="https://www.teamviewer.com/sv/teamviewers-automatiska-nedladdning/" target="_blank">Teamviewer</a> så kan vi enklare ge er support på distans med skärmdelning och fjärrstyrning.</p>
	
	<h3>Mer om Bozzanova</h3>
	<p>Bozzanova är en reklambyrå som kan hjälpa er med all form av marknadsföring och kommunikation.</p>
	<p>För mer information om våra tjänster: <strong><?php echo $this->website; ?></strong>. Följ oss gärna på våra sociala kanaler: <strong><?php echo $this->facebook; ?></strong>, <strong><?php echo $this->linkedin; ?></strong>, <strong><?php echo $this->twitter; ?></strong> och <strong><?php echo $this->instagram; ?></strong>.</p>
	
	<h3>Behöver ni ett supportavtal?</h3>
	<p>Bozzanova har ett förmånligt avtal där Bozzanova tar hand om både hemsidan och dess användare. Bozzanova sköter om uppdateringar och ser till att hemsidan är online. De som arbetar med hemsidan kan få prioriterad hjälp via telefon och e-post.</p>
	<p>Ring oss på: <strong>0521 - 66 100</strong> eller skicka e-post till <a href="mailto:info@bozzanova.se?subject=Vi behöver ett supportavtal till <?php echo $site; ?>"><strong>info@bozzanova.se</strong></a> så får ni mer informaiton.</p>
	</div>
	</div>
	
	
	
	</div> <!-- .col.left -->
	
	<div class="col right">
	<div class="postbox">
	<div class="inside">
	<h2>Vardagstips från Bozzanova</h2>
	
	<h3>Filer och Media</h3>
	<p><strong>Innan du laddar upp</strong> filer i Mediabiblioteket döper du om filen. Använd upp till 5 ord som beskriver bildens innehåll (tänk sökord) + ditt varumärke. Använd inte ÅÄÖ eller konstiga tecken (t.ex. #¤%&/) i filnamnet. Separera orden med bindestreck (-) istället för mellanslag.</p>
	<p>Ex: <em>bat-hav-sol-bozzanova-reklambyra.jpg</em></p>
	<p><strong>Efter du lattat upp</strong> filer i Mediabiblioteket skriver du en ordentlig beskrivning av filens innehåll i Rubrikfältet. Här får du använda alla typer av tecken. Smart namngivning hjälper dig att hålla ordning i Mediabiblioteket.</p>
	
	<h4>Alternativtext och bildtext</h4>
	<p>Du ska också skriva en alternativtext (alt-text) till bilden. Den ska beskriva bildens innehåll (ersätta bilden för den som inte ser) - ofta en kort beskrivning av motivet.</p>
	<p>Ex: <em>En liten båt ute på havet i mysigt solljus.</em></p>
	<p>Om du också väljer att använda en bildtext (som visas i anslutning till bilden), så ska bildtexten komplettera informationen i bilden och hur den knyter an till innehållet. Du kan också använda Bildtexten till exempelvis uppghovsrättsinformation.</p>
	<p>Glöm inte att använda sökord som hör till sidan som bilden ska finnas med på. Både alternativtext och bildtext hjälper till i bl.a. sökoptimeringen och ökar tillgängligheten på hemsidan.
	
	<h4>Bildstorlekar</h4>
	<p>Även om WordPress skapar en mängd olika bildstorlekar (beroende på tema eller egna inställningar) när man laddar upp bilder så är det bra om man kan försöka att anpassa bildens storlek till det ändamål den är tänkt för. T.ex. om en bild ska användas som en s.k. Hero-bild (en stor bild som sträcker sig över hela skärmens bredd) bör den minst vara 1920 pixlar bred.</p>
	
	<h4>Video</h4>
	<p>Det är idag viktigt att använda video i alla kanaler. Använd tjänster som t.ex. Youtube eller Vimeo för att presentera video på hemsidan. Deras servrar är anpassade för videostreaming vilket de flesta vanliga webbservrar inte är.</p>
	
	<h3>Innehållet</h3>
	<p>Varje sida ska bära sig själv - d.v.s. handla om ETT ämne. Detta ämne är den sökfras (som inkluderar böjningar och synonymer) som man ska optimera sidan mot. Gör så att innehållet passar för den tänkta målgruppen. Växla av långa texter med bilder. Baka in video-filmer i innehållet. Skriv lättsam säljande text först. När texten är klar börjar du anpassa texten med sökord och fraser. Ett sidinnehåll bör innehålla minst 300 ord. Se till att varje sida har en s.k. Call to Action (CTA) som uppfyller något av verksamhetsmålen. T.ex. boka ett möte.</p>
	<p>Enligt Google: <em>Ett användarvänligt innehåll som svarar på besökarens sökning är detsamma som ett sökoptimerat innehåll.</em></p>
	
	<h4>Viktiga sidor</h4>
	<p>Integritetspolicy - behövs enligt EU:s lagstiftning.<br>
	Information om kakor - kan behövas enligt EU:s lagstiftning.<br>
	Tillgänglighetsredogörelse - behövs om webbplatsen/organisationen finansieras av medel från EU/Staten/Kommun.<br>
	Köp och leverensvillkor - kan behövas om betalfunktioner används på hemsidan (t.ex. vid webshop)</p>
	</div>
	</div>
	
	<?php /*
	<div class="postbox">
	<div class="inside">
	<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fbozzanova.se%2F&tabs=timeline&width=500&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=261115241029502" width="100%" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
		</div>
		</div>
		*/ ?>
		
		</div> <!-- .col.right -->
		
		</div> <!-- .row -->
		
		</div>
		</div>
		<?php
	}
}