<?php
/**
 * @package   Novastyle 2020
 * @author    Jon Täng <jon@bozzanova.se>
 * @link      http://www.bozzanova.se
 * @copyright 2020 Bozzanova AB
 */

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Apply Gutenberg Theme Settings
 */
class Gutenberg {

    private static $instance = null;

    /**
     * Instance function
     *
     * @return Theme
     */
    public static function instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    /**
     * Class Construct
     *
     * @return void
     */
    public function __construct() {
        add_action( 'after_setup_theme', [ $this, 'gutenberg_setup' ] );

        add_filter( 'allowed_block_types', [ $this, 'allowed_blocks' ], 10, 2 );
    }

    /**
     * Gutenberg Setup
     *
     * @return void
     */
    public function gutenberg_setup() {

        // REMOVE EDITOR SUPPORT -------------------------------------------- //

        // Remove support for Core Block Patterns
        remove_theme_support( 'core-block-patterns' );
        // Remove support for Block Templates
        remove_theme_support( 'block-templates' );

        // Set Theme Colors
        add_theme_support( 'editor-color-palette', [] );
        add_theme_support( 'disable-custom-colors' );
        // Set Theme Gradiants
        add_theme_support( 'editor-gradient-presets', [] );
        // Remove option for custom gradiants
        add_theme_support( 'disable-custom-gradients' );
        // Remove option for custom font sizes
        add_theme_support( 'disable-custom-font-sizes' );
        // Set Theme Font Sizes
        add_theme_support( 'editor-font-sizes', [] );
        
    }

    /**
   * Allowed Gutenberg Blocks
   *
   * @param [type] $allowed_blocks
   * @param [type] $post
   * @return void
   */
  public function allowed_blocks( $allowed_blocks, $post ) {
    // echo '<pre>'; 
    // print_r( $allowed_blocks ); 
    // echo '</pre>';
    $add_allowes_blocks = [];

    $allowed_blocks = [
        // Common
        'core/paragraph',
        'core/image',
        'core/heading',
        //(Deprecated) core/subhead — Subheading
        'core/gallery',
        'core/list',
        //'core/quote',
        //'core/audio',
        //'core/cover',       // (previously core/cover-image)
        //'core/file',
        //'core/video',
        // Formatting category
        //'core/table',
        //'core/verse',
        //'core/code',
        //'core/freeform',    // — Classic
        //'core/html',        // — Custom HTML
        //'core/preformatted',
        //'core/pullquote',
        // Layout Elements category
        // 'core/buttons',
        //'core/text-columns',// — Columns
        // 'core/columns',// — Columns
        //'core/media-text',  // — Media and Text
        //'core/more',
        //'core/nextpage',     // — Page break
        //'core/separator',
        //'core/spacer',
        // Widgets category
    //   'core/shortcode',
        //'core/archives',
        //'core/categories',
        //'core/latest-comments',
        //'core/latest-posts',
        // Embeds category
        'core/embed',
        //'core-embed/twitter',
        'core-embed/youtube',
        //'core-embed/facebook',
        //'core-embed/instagram',
        //'core-embed/wordpress',
        //'core-embed/soundcloud',
        //'core-embed/spotify',
        //'core-embed/flickr',
        //'core-embed/vimeo',
        //'core-embed/animoto',
        //'core-embed/cloudup',
        //'core-embed/collegehumor',
        //'core-embed/dailymotion',
        //'core-embed/funnyordie',
        //'core-embed/hulu',
        //'core-embed/imgur',
        //'core-embed/issuu',
        //'core-embed/kickstarter',
        //'core-embed/meetup-com',
        //'core-embed/mixcloud',
        //'core-embed/photobucket',
        //'core-embed/polldaddy',
        //'core-embed/reddit',
        //'core-embed/reverbnation',
        //'core-embed/screencast',
        //'core-embed/scribd',
        //'core-embed/slideshare',
        //'core-embed/smugmug',
        //'core-embed/speaker',
        //'core-embed/ted',
        //'core-embed/tumblr',
        //'core-embed/videopress',
        //'core-embed/wordpress-tv',
        // 'core/group',
        // 'core/group',
        //'woocommerce/handpicked-products',
        //'woocommerce/product-best-sellers',
        //'woocommerce/product-category',
        //'woocommerce/product-new',
        //'woocommerce/product-on-sale',
        //'woocommerce/product-top-rated',
        //'woocommerce/products-by-attribute',
        //'woocommerce/featured-product'
        // 'core/block'
        'gravityforms/form'
    ];

    $allowed_blocks = array_merge( $allowed_blocks, $add_allowes_blocks );

    return apply_filters( 'nova\allowed_block_types', $allowed_blocks, $add_allowes_blocks );
  }
}