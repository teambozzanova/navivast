<?php
/**
* @package   Novastyle 2020
* @author    Jon Täng <jon@bozzanova.se>
* @link      http://www.bozzanova.se
* @copyright 2020 Bozzanova AB
*/

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Bozzanova {
	
	public $site = '';
	public $developer = '';
	public $website =  '';
	public $facebook = '<a href="https://www.facebook.com/bozzanova.se/" target="_blank">Facebook</a>';
	public $linkedin = '<a href="http://www.linkedin.com/company/bozzanova-ab" target="_blank">LinkedIn</a>';
	public $twitter = '<a href="https://twitter.com/bozzanova" target="_blank">Twitter</a>';
	public $instagram = '<a href="https://www.instagram.com/bozzanova.se/" target="_blank">Instagram</a>';
	
	private static $instance = null;
	
	/**
	* Instance function
	*
	* @return Theme
	*/
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	function __construct() {
		add_action( 'after_setup_theme', [ $this, 'set_site' ] );
		//$this->set_site();
		// Branding
		add_filter( 'admin_footer_text', [ $this, 'admin_footer' ]);
		add_action( 'wp_dashboard_setup', [ $this, 'dashboard_widgets' ] );
		
		// Support Page
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );

		add_action( 'admin_bar_menu', [ $this, 'customize_toolbar' ], 999 );
	}
	
	public function set_site() {
		$site = get_site_url(); 
		$site = str_replace('https://', '', $site);
		$site = str_replace('http://', '', $site);
		
		$this->site = $site;
		$this->developer = sprintf( '<a href="https://www.bozzanova.se/?utm_source=customer_site&utm_medium=website&utm_campaign=support&utm_content=%s" target="_blank">%s</a>', $site, __( 'Bozzanova Agency', 'nova' ) );
		$this->website = sprintf( '<a href="https://www.bozzanova.se/?utm_source=customer_site&utm_medium=website&utm_campaign=support&utm_content=%s" target="_blank">%s</a>', $site, __( 'Website', 'nova' ) );
	}
	
	//--------------------------------------------------------------------------------
	//	Bozzanova THEME Brading	
	//--------------------------------------------------------------------------------
	
	/*
	* Custom Backend Footer
	*/
	public function admin_footer() {
		printf( 
			'<span id="footer-thank-you">%s</span>',
			sprintf( '%s %s',
			__('Developed by', 'nova'),
			$this->developer
			)
		);
	}

	/**
	 * Add link in WP Admin Toolbar
	 * 
	 * @param array $wp_admin_bar
	 * @return array $wp_admin_bar
	 */
	public function customize_toolbar( $wp_admin_bar ) {
		$current_user = wp_get_current_user();

		// if ( $current_user->has_cap( 'administrator' ) ) {
			// echo '<pre>'; print_r( $wp_admin_bar ); echo '</pre>';
			$wp_admin_bar->add_node( [
				'id'		=> 'bozzanova-support',
				'title'		=> sprintf('<span class="ab-icon"></span><span class="ab-label">%s</span>', __( 'Bozzanova Support',  'nova' ) ),
				'href'		=> admin_url( 'index.php?page=bozzanova-support' ),
				'parent'    => 'top-secondary'
			] );
		// }	
	}
	
	/**
	* Create the function to output the contents of our Dashboard Widget.
	*/
	public function dashboard_widget() {
		// Display whatever it is you want to show.
		$current_user = wp_get_current_user();
		printf( '<div><strong>%s %s</strong><br>%s</div>', 
		__('Hello', 'nova'), 
		($current_user->firsname_name !== '') ? $current_user->firsname_name: $current_user->display_name, 
		__( 'Thanks for using a theme from Bozzanova Agency', 'nova' ) 
	);
	
	echo '<div style="border-bottom: 1px solid #eee;margin: 0 -12px;padding: 8px 12px 4px;">';
	printf( '<h2>%s</h2>', __('Need support?', 'nova' ) );
	printf( '<div>%s<br>%s: %s<br>%s: %s</div>', 
	__('You are welcome to contact us if you need any kind of help.', 'nova'),
	__('Call', 'nova' ),
	'<a href="tel:+4652166100">0521 - 66 100</a>',
	__('Email', 'nova' ),
	'<a href="mailto:support@bozzanova.se">support@bozzanova.se</a>'
);
printf('<div style="margin: .5rem 0;">%s %s %s</div>', 
__('Visit our', 'nova' ),
$this->website,
__('for more information about our services.', 'nova' )
);

printf('<div style="margin: .5rem 0;">%s: %s, %s, %s, %s</div>',
__('Follow Bozzanova on', 'nova'),
$this->facebook,
$this->linkedin,
$this->twitter,
$this->instagram
);

printf('<div style="margin: .5rem 0;">%s</div>',
sprintf('<a href="index.php?page=bozzanova-support">%s</a>', __('For more support information', 'nova') )
);
echo '</div>';
}

/**
* Add a widget to the dashboard.
*
* This function is hooked into the 'wp_dashboard_setup' action below.
*/
public function dashboard_widgets() {
	wp_add_dashboard_widget(
		'bozzanova_dashboard_widget', // Widget slug.
		__('Bozzanova Agency', 'nova' ), // Title.
		[ $this, 'dashboard_widget' ] // Display function.
	);	
}

//--------------------------------------------------------------------------------
//	Bozzanova THEME Support Page	
//--------------------------------------------------------------------------------
/** 
* Add Support Page
*/
function admin_menu() {
	add_dashboard_page(
		__('Bozzanova Support', 'nova' ),
		__('Support', 'nova' ),
		'read',
		'bozzanova-support',
		[ $this, 'render_page' ]
	);
}

/** 
* Render Support Page
*/
function render_page() {
	$site = get_site_url(); 
	$site = str_replace('https://', '', $site);
	$site = str_replace('http://', '', $site);
	//get_template_part('parts/admin/support');
	
	$urls = [
		'support' => 'https://support.bozzanova.info/wp/wp-json/wp/v2/pages/2',
		'elementor' => 'https://support.bozzanova.info/wp/wp-json/wp/v2/pages/12',
		'tips' => 'https://support.bozzanova.info/wp/wp-json/wp/v2/pages/9',
	];
	
	$data = [];

	foreach ($urls as $url ) {
		$response = wp_remote_get( esc_url_raw( $url ) );
		$body = json_decode( wp_remote_retrieve_body( $response ), true );

		$data[] = [
			'id' => $body['id'],
			'slug' => $body['slug'],
			'title' => $body['title']['rendered'],
			'content' => $body['content']['rendered']
		];
	}


	?>
	<style>
	.novastyle {
		font-size: 16px;
	}

	
	.novastyle .inside {
		padding: 1rem 2rem;
	}
	.novastyle > h1 {
		display: flex;
		align-items: center;
	}

	.novastyle .icon {
		position:relative;
		margin-right: 20px;
		width: 40px;
		height: 40px;
		display: inline-block;		
	}

	.novastyle .icon svg {
		width: 100%;
		height: auto;
		
	}
	
	/*.novastyle .icon:before {
		position: absolute;
		padding: 1px 0;
		width: 50px;
		height: 50px;
		display: block;
		top: 0;
		content: '';
		
		background-image: url('data:image/svg+xml;utf8,<svg aria-hidden="true" focusable="false" data-icon="bozzanova" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.46 283.46"><rect fill="currentColor" y="261.66" width="239.17" height="21.8"/><rect fill="currentColor" y="218.05" width="283.46" height="21.8"/><rect fill="currentColor" y="174.44" width="283.46" height="21.8"/><rect fill="currentColor" y="130.83" width="256.89" height="21.8"/><rect fill="currentColor" y="87.22" width="283.46" height="21.8"/><rect fill="currentColor" y="43.61" width="283.46" height="21.8"/><rect fill="currentColor" width="239.17" height="21.8"/></svg>');
		background-repeat: no-repeat;
		background-size: contain;
	}*/

	.novastyle h1 {
		font-size: 2.5rem;
		margin: 1rem 0;
	}
	.novastyle h2,
	.postbox .inside h2 {
		font-size: 1.8rem;
		margin-top: 1rem;
		margin-bottom: .75rem;
		padding: 0;
	}
	.novastyle h3,
	.novastyle h4 {
		font-weight: 400;
	}
	
	.novastyle h3 {
		font-size: 1.3rem;
		margin-top: 1.5rem;
		margin-bottom: .25rem;
	}
	.novastyle h4 {
		font-size: 1.1rem;
		margin-top: 1.25rem;
		margin-bottom: .125rem;
	}
	
	.novastyle p {
		margin-top: 0;
		font-size: 1rem;
	}
	
	.nova-tab-content {
		display: none;

		max-inline-size: 800px;
		
		background-color: #fff;
		padding-block: 1rem;
		padding-inline: 2rem;
	}

	.nova-tab-content:target {
		display: block;
	}

	</style>
	<div class="wrap novastyle">
		<?php
		echo '<h1><span class="icon"><svg aria-hidden="true" focusable="false" data-icon="bozzanova" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.46 283.46"><rect fill="currentColor" y="261.66" width="239.17" height="21.8"/><rect fill="currentColor" y="218.05" width="283.46" height="21.8"/><rect fill="currentColor" y="174.44" width="283.46" height="21.8"/><rect fill="currentColor" y="130.83" width="256.89" height="21.8"/><rect fill="currentColor" y="87.22" width="283.46" height="21.8"/><rect fill="currentColor" y="43.61" width="283.46" height="21.8"/><rect fill="currentColor" width="239.17" height="21.8"/></svg></span>Bozzanova Support</h1>';
	
	// $response = wp_remote_get( esc_url_raw( $urls['support'] ) );
	// $body = json_decode( wp_remote_retrieve_body( $response ), true );
	// $body = $response['body'];

	echo '<div class="nav-tab-wrapper">';
	foreach ( $data as $d ) {
		printf(
			'<a id="tab-%1$s" class="nav-tab" href="#%1$s">%2$s</a>',
			$d['slug'],
			$d['title']
		);
	}
	echo '</div>';
	// echo '<pre>'; print_r( $body ); echo '</pre>';
	foreach ( $data as $d ) {
		printf( 
			'<div id="%s" class="nova-tab-content"><h1>%s</h1><div>%s</div></div>',
			$d['slug'],
			$d['title'],
			$d['content']
		); 
	}
		
	}
}