<?php
/**
 * Class Theme
 */
namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Theme {

    private static $instance = null;

    /** 
     * Class Instace
     * 
     * @return $instance
     */
    public static function instance() : Theme {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
    
    /**
     * Class Construct
     * 
     * @return void
     */
    public function __construct() {
        // add_action( 'hook', [ $this, 'function' ] );
        add_action( 'after_setup_theme', [ $this, 'theme_setup' ], 5 );
        
        $this->init_components();
    }

    /**
     * Theme Setup
     * 
     * @return void
     */
    public function theme_setup() {
        
        // Text Domain
        load_theme_textdomain( 'nova', get_template_directory() . '/languages' );

        // Theme Support
        $theme_support = [
            'menus',
            'post-thumbnails' => true, //[ 'page', 'post' ],
			'automatic-feed-links' => true,
			'title-tag' => true,
			'html5' => [
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            ],
		 	'custom-logo' => [
					'height'      => 100,
					'width'       => 350,
					'flex-height' => true,
					'flex-width'  => true,
			]
		];

        // Loop values
        foreach ( $theme_support as $key => $value ) {
            // echo '<pre>'; print_r( $value ); echo '</pre>';
            if ( is_array( $value ) ) {
                add_theme_support( $key, $value );
            } else {
                add_theme_support( $key );
            }
    
        }

        register_nav_menus( array(
            'primary_menu' => __( 'Primary Menu', 'nova' ),
            'footer_menu'  => __( 'Footer Menu', 'nova' ),
        ) );

        
    }

    /**
     * Init Theme Dependancies
     */
    public function init_components() {
        
        new Bozzanova();
        new Users();
        new Gutenberg();
        new Media();
        new Museum();
        new Event();
        new Activity();
        // new News();

        $active_plugins = get_option('active_plugins');

        // ACF
        if ( in_array( 'advanced-custom-fields-pro/acf.php', $active_plugins ) || in_array( 'advanced-custom-fields/acf.php', $active_plugins ) ) {
            new ACF();
        }
        
        // Elementor
        if ( in_array( 'elementor-pro/elementor-pro.php', $active_plugins ) || in_array( 'elementor/elementor.php', $active_plugins ) ) {
            new Elementor();
        }

        // echo '<pre>'; print_r( $theme ); echo '</pre>';
    }
}
