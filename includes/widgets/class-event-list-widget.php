<?php

namespace novatheme\Widgets;

// Elementor
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Control_Image_Dimensions;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use WP_Query;
use Date;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Event_List_Widget extends Widget_Base {

    public function get_name() {
        return 'nova-event-list';
    }

    public function get_title() {
        return __( 'Event List', 'nova' );
    }

    public function get_icon() {
        return 'fas fa-th-large';
    }

    public function get_custom_help_url() {}

    public function get_categories() {
        return [ 'nova_elements' ];
    }

    public function get_keywords() {
        return array( 'image', 'box', 'info', 'cta' );
    }

    public function get_script_depends() {
        // wp_register_script( 'widget-script', JS_URL . 'widget-script.js' );
            
        return [
            // 'widget-script'
        ];
    }

    public function get_style_depends() {

        wp_register_style( 'event-list-widget', CSS_URL . 'event-list-widget.css' );
            
        return [
            'event-list-widget'
        ];
    }

    protected function register_controls() {

        //  QUERY
        $this->start_controls_section(
            'content_query_section',
            [
                'label' => esc_html__( 'Query', 'nova' ),
            ]
        );

		$this->add_control(
			'query_posttype',
			[
				'type' => \Elementor\Controls_Manager::SELECT,
				'label' => esc_html__( 'Post type', 'nova' ),
				'options' => [
					// 'default' => esc_html__( 'Default', 'nova' ),
					'event' => esc_html__( 'Event', 'nova' ),
					'activity' => esc_html__( 'Activity', 'nova' ),
				],
				'default' => 'event',
			]
		);


        $this->add_control(
			'query_orderby',
			[
				'type' => \Elementor\Controls_Manager::SELECT,
				'label' => esc_html__( 'Orderby', 'nova' ),
				'options' => [
					// 'default' => esc_html__( 'Default', 'nova' ),
					'event_date' => esc_html__( 'Event Date', 'nova' ),
					'post_date' => esc_html__( 'Post Date', 'nova' ),
					'title' => esc_html__( 'Title', 'nova' ),
				],
				'default' => 'event_date',
			]
		);

        $this->add_control(
			'query_order',
			[
				'type' => \Elementor\Controls_Manager::SELECT,
				'label' => esc_html__( 'Order', 'nova' ),
				'options' => [
					'asc' => esc_html__( 'ASC', 'nova' ),
					'desc' => esc_html__( 'DESC', 'nova' ),
				],
				'default' => 'desc',
			]
		);

        $this->end_controls_section();

        //  LAYOUT
        $this->start_controls_section(
            'content_layout_section',
            [
                'label' => esc_html__( 'Layout', 'nova' ),
            ]
        );

		$this->add_control(
			'content_layout_style',
			[
				'label' => esc_html__( 'Alignment', 'nova' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'rows' => [
						'title' => esc_html__( 'Rows', 'nova' ),
						'icon' => 'eicon-post-list',
					],

					'cols' => [
						'title' => esc_html__( 'Columns', 'nova' ),
						'icon' => 'eicon-posts-grid',
					],
					
				],
				'default' => 'rows',
				'toggle' => true,
			]
		);


        $this->add_responsive_control(
			'block_cols',
			[
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label' => esc_html__( 'Columns', 'nova' ),
				'placeholder' => '0',
				'min' => 0,
				'max' => 4,
				'step' => 1,
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'desktop_default' => 3, 
                'tablet_default' => 2,
                'mobile_default' => 1,             
                'selectors' => [
					'{{WRAPPER}} .event-list.cols' => '--cols: repeat({{SIZE}}, 1fr);'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
                'condition' => [
                    'content_layout_style' => 'cols',
                ]
			]
		);


        $this->add_responsive_control(
			'block_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Gap', 'nova' ),
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .event-list' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

        $this->add_control(
			'layout_show_image',
			[
				'label' => __( 'Show Image', 'nova' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'nova' ),
				'label_off' => __( 'Hide', 'nova' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->add_group_control(
            \Elementor\Group_Control_Image_Size::get_type(),
            [
                'name' => 'image', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `thumbnail_size` and `thumbnail_custom_dimension`.
                'exclude' => ['custom'],
                'include' => [],
                'default' => 'map_icon',
                'condition' => [
                    'layout_show_image' => 'yes',
                ],
            ]
        );

        $this->add_control(
			'image_width',
			[
				'label' => esc_html__( 'Width', 'nova' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ '%' ],
				'range' => [
					'%' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'default' => [
					'unit' => '%',
					'size' => 50,
				],
				'selectors' => [
					'{{WRAPPER}} .event-post-image' => '--image-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .event-post-body' => '--body-size: calc(100% - {{SIZE}}{{UNIT}});',
				],
				'condition' => [
                    'layout_show_image' => 'yes',
                    'content_layout_style' => 'rows',
                ],
			]
		);

		$this->add_control(
			'image_style',
			[
				'label' => esc_html__( 'Image Style', 'nova' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '1:1',
				'options' => [
					'1'  => esc_html__( '1:1', 'nova' ),
					'4 / 3' => esc_html__( '4:3', 'nova' ),
					'16 / 9' => esc_html__( '16:9', 'nova' )
				],
				'selectors' => [
					'{{WRAPPER}} .event-post-image img' => '--image-ratio: {{OPTION}};'
				],
				'condition' => [
                    'layout_show_image' => 'yes',
                ],
			]
		);


        $this->add_responsive_control(
			'post_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Post Content Gap', 'nova' ),
                'condition' => [
                    'layout_show_image' => 'yes',
                ],
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .event-post' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

        // $this->add_control(
		// 	'layout_show_title',
		// 	[
		// 		'label' => __( 'Show Title', 'nova' ),
		// 		'type' => \Elementor\Controls_Manager::SWITCHER,
		// 		'label_on' => __( 'Show', 'nova' ),
		// 		'label_off' => __( 'Hide', 'nova' ),
		// 		'return_value' => 'yes',
		// 		'default' => 'yes',
		// 	]
		// );

        $this->add_control(
			'layout_title_size',
			[
				'label' => __( 'Title HTML Tag', 'nova' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'h1' => 'H1',
					'h2' => 'H2',
					'h3' => 'H3',
					'h4' => 'H4',
					'h5' => 'H5',
					'h6' => 'H6',
				],
				'default' => 'h3'
                // 'condition' => [
                //     'layout_show_title' => 'yes'
                // ]
			]
		);

        $this->add_control(
			'layout_show_date',
			[
				'label' => __( 'Show Date', 'nova' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'nova' ),
				'label_off' => __( 'Hide', 'nova' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$this->add_control(
			'layout_date_format',
			[
				'label' => esc_html__( 'Date Format', 'nova' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'Y-m-d',
				'options' => [
					'Y-m-d'  => esc_html__( '2021-12-31', 'nova' ),
					'j F, Y' => esc_html__( '31 december, 2021', 'nova' ),
					
				],
				// 'selectors' => [
				// 	'{{WRAPPER}} .event-post-image img' => '--image-ratio: {{OPTION}};'
				// ],
				'condition' => [
                    'layout_show_date' => 'yes',
                ],
			]
		);

        $this->add_control(
			'layout_show_content',
			[
				'label' => __( 'Show Content', 'nova' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'nova' ),
				'label_off' => __( 'Hide', 'nova' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->add_control(
			'layout_wordcount',
			[
				'type' => \Elementor\Controls_Manager::TEXT,
				'label' => esc_html__( 'Content Word Lenght', 'nova' ),
				// 'placeholder' => esc_html__( 'Enter your title', 'nova' ),
                'condition' => [
                    'layout_show_content' => 'yes',
                ],
			]
		);

        $this->add_responsive_control(
			'post_content_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Image Content Gap', 'nova' ),
                'condition' => [
                    'layout_show_title' => 'yes',
                    'layout_show_content' => 'yes',
                ],
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .event-post-body' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

		$this->add_responsive_control(
			'post_body_padding',
			[
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'label' => esc_html__( 'Padding', 'nova' ),
				'size_units' => [ 'px', 'rem', 'em' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
                    'top' => 0.5,
                    'right' => 1,
                    'bottom' => 0.5,
                    'left' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'top' => 0.5,
                    'right' => 1,
                    'bottom' => 0.5,
                    'left' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'top' => 0.5,
                    'right' => 1,
                    'bottom' => 0.5,
                    'left' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .event-post-body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
                'dynamic' => [
                    'active' => true,
                ],
			]
		);

        $this->end_controls_section();

        // STYLE
        $this->start_controls_section(
            'section_style',
            [
                'label' => esc_html__( 'Style', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

		$this->add_control(
			'background_color',
			[
				'label' => esc_html__( 'Title Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .event-post' => 'background-color: {{VALUE}}',
				],
			]
		);

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Title Typography', 'nova' ),
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .event-post-title',
			]
		);

        $this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Title Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .event-post-title a' => 'color: {{VALUE}}',
				],
			]
		);

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Meta Typography', 'nova' ),
				'name' => 'meta_typography',
				'selector' => '{{WRAPPER}} .event-post-meta',
			]
		);

        $this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Meta Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .event-post-meta' => 'color: {{VALUE}}',
				],
			]
		);

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Text Typography', 'nova' ),
				'name' => 'content_typography',
				'selector' => '{{WRAPPER}} .event-post-content',
			]
		);

        $this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Text Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .event-post-content' => 'color: {{VALUE}}',
				],
			]
		);

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Label', 'nova' ),
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .facility-label',
			]
		);

        $this->end_controls_section();

    }

    public function create_posts( $settings ) {
        $today = date( 'Ymd' );

        $post_type =  ( $settings['query_posttype'] ) ?: 'event';
        // $taxonomy = 'event_category';
        $terms = ( $settings['terms'] ) ?: false;
        $posts_per_page = ( $settings['posts_per_page'] ) ?: -1;
        $orderby = ( $settings['orderby'] ) ?: [ 'date' => 'DESC' ];

        // echo '<pre>'; print_r( $today ); echo '</pre>';

        $query_args = [
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page
        ];

        $query_args = array_merge( 
            $query_args,
            [
                'meta_key' => 'start_date', 
                'orderby' => 'meta_value', 
                'order' => 'ASC',
                // 'meta_query' => array(
                // 	'relation' => 'AND',
                // 	array(
                // 			'key'     => 'start_date',
                // 			'compare' => 'EXISTS',
                // 	),
                //     array(
                //         'key' => 'start_date',
                //         'value'=> $today,
                //         'compare' => '>=',
				// 	),
				// 	array(
                //         'key' => 'end_date',
                //         'value'=> $today,
                //         'compare' => '>=',
                //     )
                // )
            ]
        );

        // if ( 0 != $terms ) {
        //     $query_args = array_merge(
        //         $query_args, [
        //             'tax_query' => [
        //                 [
        //                     'taxonomy' => $taxonomy,
        //                     'field'    => 'id',
        //                     'terms'    => $terms,
        //                 ],
        //             ],
        //             'orderby' => $orderby
        //         ]
        //     );
        // }

        $q = new \WP_Query( $query_args );

        if ( ! $q->have_posts() ) 
            return false;

        $posts = [];

        foreach ( $q->posts as $p ) {
            $start = get_field( 'start', $p->ID );
            $end = get_field( 'end', $p->ID );

            $posts[] = (object) [
                'id' => $p->ID,
                'url' => get_permalink( $p->ID ) ?: false,
                'title' => $p->post_title ?: false,
                'content' => $p->post_content ?: false,
                'start_date' => get_field( 'start_date', $p->ID ) ?: false,
                'end_date' => get_field( 'end_date', $p->ID ) ?: false,
                'start_time' => get_field( 'start_time', $p->ID ) ?: false,
                'end_time' => get_field( 'end_time', $p->ID ) ?: false
            ];
        }

        return $posts;
    }

    protected function render() {
        $today = date( 'Ymd' );
        $settings = $this->get_settings_for_display();

        $show_image = ( 'yes' == $settings['layout_show_image'] ) ? true : false;
        $image_size = $settings['image_size'] ?: 'medium';
        if ( 'custom' == $image_size ) {
            $image_size = [ $image_custom_dimension['width'], $image_custom_dimension['height'] ];
        }

        $show_title = ( 'yes' == $settings['layout_show_title'] ) ? true : false;
        $title_level  = ( $settings['layout_title_size'] ) ?: 'h2';
        
        $show_date = ( 'yes' == $settings['layout_show_date'] ) ? true : false;
		$dateformat = ( $settings['layout_date_format'] ) ?: 'Y-m-d';
        $show_content = ( 'yes' == $settings['layout_show_content'] ) ? true : false;
        $word_count = ( $settings['layout_wordcount'] ) ?: 25;
        $more_text = ( $settings['more_text'] ) ?: '';

        $posts = $this->create_posts( $settings );

        if ( ! is_array( $posts ) ) return;

		// echo '<pre>'; print_r( $settings['content_layout_style'] ); echo '</pre>';

        printf( 
			'<div class="event-list %s">',
			$settings['content_layout_style']
		);
		
        foreach ( $posts as $p ) {
			$start_date = $end_date = $start_time = $end_time = $show_post = false;

			if ( ( $p->start_date >= $today ) ) {
				$show_post = true;
			} elseif (( $p->end_date >= $today )) {
				$show_post = true;
			}
			
			if ( $show_post ) {

				if ( $p->start_date ) {
				
					if ( $p->start_date <= $today ) {
						$start_date = __('Ongoing', 'nova');
						// $start_time = false;
					} else {
						$start_date = date_i18n( $dateformat, strtotime( $p->start_date) );
						
					}
				}
				
				// echo '<pre>'; var_dump( $p->end_date ); echo '</pre>';
				
				if ( $p->end_date ) {
					
					if ( $p->end_date == $today ) {
						$end_date = __('Ends today', 'nova');
						// $end_time = false;
					} else {
						$end_date = date_i18n( $dateformat, strtotime( $p->end_date ) );
						// $end_time = $p->end_time;
					}
				}

				if ( $p->start_time ) {
					$start_time = $p->start_time;
				}

				if ( $p->end_time ) {
					$end_time = $p->end_time;
				}
				
				printf( '<div id="event-post-%s" class="event-post">', $p->id );
				
				if ( has_post_thumbnail( $p->id ) && $show_image ) {
					printf( 
						'<div class="event-post-image">%s</div>',
						get_the_post_thumbnail( $p->id, $image_size )
					);
				}

				echo '<div class="event-post-body">';
					// if ( $show_title ) { 
						printf( 
							'<%1$s class="event-post-title"><a href="%3$s">%2$s</a></%1$s>', 
							$title_level, 
							$p->title,
							$p->url
						);
					// }

					if ( $show_date ) {
						printf( 
							'<div class="event-post-meta">%s%s</div>',
							sprintf( 
								'<div class="date">%s: %s%s</div>', 
								__( 'Date', 'nova' ),
								$start_date,
								( $end_date && ( $end_date != $start_date ) ) ? ' - ' . $end_date : ''
							),
							( $start_time ) ? sprintf( 
								'<div class="time">%s: %s%s</div>', 
								__( 'Time', 'nova' ),
								$start_time,
								( $end_time ) ? ' - ' . $end_time : ''
							) : ''
						);
					}
					if ( $show_content ) {
						printf( '<div class="event-post-content">%s</div>', wp_trim_words( $p->content, $word_count, $more_text ) );
					}
				echo '</div>';
				echo '</div>';
			}
        }
        echo '</div>';
    }

	protected function content_template() {}
}