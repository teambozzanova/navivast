<?php
/**
 * single-gmap.php
 * A Elementor Widget for displaying map-filter
 * 
 * @package		NovaTheme
 * @author		Jon Täng <jon@bozzanova.se>
 * @link	  	https://www.bozzanovaPOI Google Map * @copyright	Bozzanova AB
 */

namespace novatheme\Widgets;

// Elementor
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use WP_Query;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class POI_Filter extends Widget_Base {

    public $area_posts;
    public $property_posts;

    public function __construct( $data = [], $args = null ) {
        parent::__construct( $data, $args );

        add_action( 'elementor/frontend/before_render', [ $this, 'create_area_posts' ] );
        // add_action( 'elementor/frontend/before_render', [ $this, 'create_property_posts' ] );

        // add_action( 'elementor/editor/after_ register_scripts', [ $this, 'load_editor_scripts' ] );
        // add_action( 'elementor/editor/before_enqueue_scripts', [ $this, 'load_editor_scripts' ] );
        // add_action( 'elementor/editor/before_enqueue_scripts', [ $this, 'editor_scripts' ] );
        // add_action( 'elementor/preview/enqueue_styles', [ $this, 'load_editor_scripts' ] );
        // add_action( 'elementor/frontend/after_register_scripts', array( $this, 'frontend_scripts' ) ); // Register Frontend Scripts
    }

    public function get_name() {
        return 'map-filter';
    }

    public function get_title() {
        return __( 'Map Filter (for Google Maps)', 'nova' );
    }

    public function get_icon() {
        return 'eicon-map-pin';
    }

    public function get_custom_help_url() {}

    public function get_categories() {
        return [ 'nova_elements' ];
    }

    public function get_keywords() {
        return [
            __( 'map-filter', 'nova' ), 
            __( 'map listing', 'nova' )
        ];
    }
    
    public function get_script_depends() {
        
        // if ( DEV ) {
            $file = 'map-filter.js';
        // } else {
        //     $file = 'min/map-filter.min.js';
        // }

        wp_register_script( 'select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', [] );
        wp_register_script( 'map-filter', JS_URL . $file, [ 'jquery', 'select2' ], 90 );
            
        return [
			'jquery',
            'select2',
            'map-filter'
            // 'widget-script'
        ];
    }

    public function get_style_depends() {

        wp_register_style( 'select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' );
        wp_register_style( 'map-filter', CSS_URL . 'map-filter.css' );
            
        return [
			// 'select2',
            'map-filter'
        ];
    }

    protected function register_controls() {

        // CONTENT

        $this->start_controls_section(
            '_content_title',
            [
                'label' => esc_html__( 'Dialog', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
			'title',
			[
				'label' => esc_html__( 'Title Text', 'nova' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Filter', 'nova' ),
				'placeholder' => esc_html__( 'Type your title here', 'nova' ),
			]
		);

        $this->end_controls_section();

        $this->start_controls_section(
            '_content_button',
            [
                'label' => esc_html__( 'Open Button', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
			'_content_open_button_heading',
			[
				'label' => esc_html__( 'Open Button', 'nova' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

        $this->add_control(
			'open_button',
			[
				'label' => esc_html__( 'Button Text', 'nova' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Open Filter', 'nova' ),
				'placeholder' => esc_html__( 'Type your text here', 'nova' ),
			]
		);

        $this->add_control(
			'_content_close_button_heading',
			[
				'label' => esc_html__( 'Close Button', 'nova' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

        $this->add_control(
			'close_button',
			[
				'label' => esc_html__( 'Button Text', 'nova' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Close Filter', 'nova' ),
				'placeholder' => esc_html__( 'Type your text here', 'nova' ),
			]
		);

        $this->add_control(
			'_content_clear_button_heading',
			[
				'label' => esc_html__( 'Clear Button', 'nova' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

        $this->add_control(
			'clear_button',
			[
				'label' => esc_html__( 'Button Text', 'nova' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Clear Filter', 'nova' ),
				'placeholder' => esc_html__( 'Type your text here', 'nova' ),
			]
		);

        $this->end_controls_section();

        // STYLE

        // Open Button Style
        $this->start_controls_section(
            '_style_open_button',
            [
                'label' => esc_html__( 'Open Button', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        // type
        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Typography', 'nova' ),
				'name' => 'open_btn_type',
				'show_label' => true,
				'label_block' => true,
				'description' => '',
				'separator' => 'default',
				'selector' => '{{WRAPPER}} .btn-open-filter-dialog',
			]
		);
        // padding
        $this->add_responsive_control(
			'open_btn_padding',
			[
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'label' => esc_html__( 'Padding', 'nova' ),
				'size_units' => [ 'px', 'rem', 'em' ],
                'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
                    'top' => 1,
                    'right' => 1,
                    'bottom' => 1,
                    'left' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'top' => 1,
                    'right' => 1,
                    'bottom' => 1,
                    'left' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'top' => 1,
                    'right' => 1,
                    'bottom' => 1,
                    'left' => 1,
					'unit' => 'rem',
				],
                'default' => [
					'top' => 1,
                    'right' => 2,
                    'bottom' => 1,
                    'left' => 2,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .btn-open-filter-dialog' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);

        // border
        $this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'open_btn_border',
				'label' => esc_html__( 'Border', 'nova' ),
				'selector' => '{{WRAPPER}} .btn-open-filter-dialog',
			]
		);

        $this->add_responsive_control(
			'open_btn_radius',
			[
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'label' => esc_html__( 'Border Radius', 'nova' ),
				'size_units' => [ 'px', 'rem', 'em' ],
                'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
                    'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
                'default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .btn-open-filter-dialog' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);

        // background (default / hover)
        // color (default / hover)
        $this->start_controls_tabs(
			'_open_btn_style_tabs'
		);

        $this->start_controls_tab(
			'_open_btn_style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'nova' ),
			]
		);

        // background color
        $this->add_control(
			'open_btn_default_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn-open-filter-dialog' => 'background-color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);

        // color
        $this->add_control(
			'open_btn_default_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn-open-filter-dialog' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);

        $this->end_controls_tab();

        // STATE HOVER
		$this->start_controls_tab(
			'_open_btn_style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'nova' ),
			]
		);
        // background color
        $this->add_control(
			'open_btn_hover_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn-open-filter-dialog:hover' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .btn-open-filter-dialog:focus' => 'background-color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);
        // color
        $this->add_control(
			'open_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn-open-filter-dialog:hover' => 'color: {{VALUE}}',
					'{{WRAPPER}} .btn-open-filter-dialog:focus' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);
        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        // Dialog Style
        $this->start_controls_section(
            '_style_dialog',
            [
                'label' => esc_html__( 'Dialog', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        // Legend
        $this->add_control(
			'_style_title_heading',
			[
				'label' => esc_html__( 'Dialog Title', 'nova' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        // type
        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Typography', 'nova' ),
				'name' => 'title_type',
				'show_label' => true,
				'label_block' => true,
				'description' => '',
				'separator' => 'default',
				'selector' => '{{WRAPPER}} .dialog-title',
			]
		);
        // color
        $this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .dialog-title' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);
        

        // Legend
        $this->add_control(
			'_style_legend_heading',
			[
				'label' => esc_html__( 'Title', 'nova' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        // type
        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Typography', 'nova' ),
				'name' => 'legend_type',
				'show_label' => true,
				'label_block' => true,
				'description' => '',
				'separator' => 'default',
				'selector' => '{{WRAPPER}} .legend',
			]
		);
        // color
        $this->add_control(
			'legend_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .legend' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);
        
        // Input
        $this->add_control(
			'_style_input_heading',
			[
				'label' => esc_html__( 'Input', 'nova' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        
        // type
        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Typography', 'nova' ),
				'name' => 'input_type',
				'show_label' => true,
				'label_block' => true,
				'description' => '',
				'separator' => 'default',
				'selector' => '{{WRAPPER}} .form-label, {{WRAPPER}} .form-input',
			]
		);

        // color
        $this->add_control(
			'input_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .form-label' => 'color: {{VALUE}}',
					'{{WRAPPER}} .form-input' => 'color: {{VALUE}}',
				],
			]
		);

        $this->end_controls_section();

        // Close Button Style
        $this->start_controls_section(
            '_style_close_button',
            [
                'label' => esc_html__( 'Dialog Buttons', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        // padding
        $this->add_responsive_control(
			'close_btn_padding',
			[
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'label' => esc_html__( 'Padding', 'nova' ),
				'size_units' => [ 'px', 'rem', 'em' ],
                'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
                    'top' => 1,
                    'right' => 1,
                    'bottom' => 1,
                    'left' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'top' => 1,
                    'right' => 1,
                    'bottom' => 1,
                    'left' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'top' => 1,
                    'right' => 1,
                    'bottom' => 1,
                    'left' => 1,
					'unit' => 'rem',
				],
                'default' => [
					'top' => 1,
                    'right' => 2,
                    'bottom' => 1,
                    'left' => 2,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .btn-close-filter-dialog' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .btn-clear-filter-dialog' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);

        $this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'close_btn_border',
				'label' => esc_html__( 'Border', 'nova' ),
				'selector' => '{{WRAPPER}} .btn-close-filter-dialog, {{WRAPPER}} .btn-clear-filter-dialog',
			]
		);

        $this->add_responsive_control(
			'close_btn_radius',
			[
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'label' => esc_html__( 'Border Radius', 'nova' ),
				'size_units' => [ 'px', 'rem', 'em' ],
                'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
                    'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
                'default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .btn-close-filter-dialog' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .btn-clear-filter-dialog' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);

        // type
        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Typography', 'nova' ),
				'name' => 'close_btn_type',
				'show_label' => true,
				'label_block' => true,
				'description' => '',
				'separator' => 'default',
				'selector' => '{{WRAPPER}} .btn-close-filter-dialog, {{WRAPPER}} .btn-clear-filter-dialog',
			]
		);

        $this->add_control(
			'_style_close_button_heading',
			[
				'label' => esc_html__( 'Close Button', 'nova' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        
        // backgroound (default / hover)
        // color (default / hover)
        $this->start_controls_tabs(
			'_close_btn_style_tabs'
		);

        $this->start_controls_tab(
			'_close_btn_style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'nova' ),
			]
		);

        // background color
        $this->add_control(
			'close_btn_default_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn-close-filter-dialog' => 'background-color: {{VALUE}}',
					// '{{WRAPPER}} .btn-clear-filter-dialog' => 'background-color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);

        // color
        $this->add_control(
			'close_btn_default_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn-close-filter-dialog' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .btn-clear-filter-dialog' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);

        $this->end_controls_tab();

        // STATE HOVER
		$this->start_controls_tab(
			'_close_btn_style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'nova' ),
			]
		);
        // background color
        $this->add_control(
			'close_btn_hover_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn-close-filter-dialog:hover, {{WRAPPER}} .btn-close-filter-dialog:focus' => 'background-color: {{VALUE}}',
					// '{{WRAPPER}} .btn-clear-filter-dialog:hover, {{WRAPPER}} .btn-clear-filter-dialog:focus' => 'background-color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);
        // color
        $this->add_control(
			'close_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn-close-filter-dialog:hover, {{WRAPPER}} .btn-close-filter-dialog:focus' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .btn-clear-filter-dialog:hover, {{WRAPPER}} .btn-clear-filter-dialog:focus' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);
        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_control(
			'_style_clear_button_heading',
			[
				'label' => esc_html__( 'Clear Button', 'nova' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        
        // backgroound (default / hover)
        // color (default / hover)
        $this->start_controls_tabs(
			'_clear_btn_style_tabs'
		);

        $this->start_controls_tab(
			'_clear_btn_style_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'nova' ),
			]
		);

        // background color
        $this->add_control(
			'clear_btn_default_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					// '{{WRAPPER}} .btn-close-filter-dialog' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .btn-clear-filter-dialog' => 'background-color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);

        // color
        $this->add_control(
			'clear_btn_default_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					// '{{WRAPPER}} .btn-close-filter-dialog' => 'color: {{VALUE}}',
					'{{WRAPPER}} .btn-clear-filter-dialog' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);

        $this->end_controls_tab();

        // STATE HOVER
		$this->start_controls_tab(
			'_clear_btn_style_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'nova' ),
			]
		);
        // background color
        $this->add_control(
			'clear_btn_hover_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					// '{{WRAPPER}} .btn-close-filter-dialog:hover, {{WRAPPER}} .btn-close-filter-dialog:focus' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .btn-clear-filter-dialog:hover, {{WRAPPER}} .btn-clear-filter-dialog:focus' => 'background-color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);
        // color
        $this->add_control(
			'clear_btn_hover_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					// '{{WRAPPER}} .btn-close-filter-dialog:hover, {{WRAPPER}} .btn-close-filter-dialog:focus' => 'color: {{VALUE}}',
					'{{WRAPPER}} .btn-clear-filter-dialog:hover, {{WRAPPER}} .btn-clear-filter-dialog:focus' => 'color: {{VALUE}}',
					// '{{WRAPPER}} .filter-toggler a' => 'color: {{VALUE}}',
				],
			]
		);
        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

    }


    public function create_area_posts() {
        
        // delete_transient( 'area_query' );

        if ( false === ( $q = get_transient( 'area_query' ) ) ) {
            // It wasn't there, so regenerate the data and save the transient
            $area_query_args = [
                'post_type' => 'area',
                'posts_per_page' => -1,
                'post_status' => 'publish'
            ];
            
            $q = new WP_Query( $area_query_args );

            // Store query
            set_transient( 'area_query', $q, 12 * HOUR_IN_SECONDS );
        }

        $this->area_posts = $q; 
    }

    public function create_property_posts() {
        if ( false === ( $q = get_transient( 'property_query' ) ) ) {
            // It wasn't there, so regenerate the data and save the transient
            $property_query_args = [
                'post_type' => 'property',
                'posts_per_page' => -1,
                'post_status' => 'publish'
            ];
            
            $q = new WP_Query( $property_query_args );

            // Store query
            set_transient( 'property_query', $q, 12 * HOUR_IN_SECONDS );
        }

        $this->property_posts = $q; 
    }


    public function area_filter() {

        $filter = false;
        // if ( $q = $this->area_posts ) {}
        // else {

        //     if ( false === ( $q = get_transient( 'area_query' ) ) ) {
        //         // It wasn't there, so regenerate the data and save the transient
        //         $area_query_args = [
        //             'post_type' => 'area',
        //             'posts_per_page' => -1,
        //             'post_status' => 'publish'
        //         ];

        //         $q = new WP_Query( $area_query_args );

        //         // Store query
        //         set_transient( 'area_query', $q, 12 * HOUR_IN_SECONDS );
        //     }
        // }

        // if ( 0 < count( $q->posts ) ) {

        //     $filter = [];
            
        //     foreach ( $q->posts as $p ) {
                
        //         $filter[] = [
        //             'id' => $p->ID,
        //             'slug' => $p->post_name,
        //             'label' => $p->post_title
        //         ];

        //     }
        // }

        // wp_reset_query();

        // return $filter;

        // echo '<pre>'; print_r( $filter ); echo '</pre>';

    }
    public function type_filter() {
        $filter = false;

        // delete_transient( 'property_type');
        // if ( false === ( $property_type = get_transient( 'property_type' ) ) ) {
            $museum_type = get_terms('museum_type', [
                'hide_empty' => false
            ]);

            // set_transient( 'museum_type', $museum_type, 12 * HOUR_IN_SECONDS );
        // }

        if ( 0 < count( $museum_type ) ) {

            $filter = [];

            foreach ($museum_type as $p ) {
                $filter[] = [
                    'id' => $p->term_id,
                    'slug' => $p->slug,
                    'label' => $p->name,
                ];
            }
        }
        
        // echo '<pre>'; print_r( $filter ); echo '</pre>';

        return $filter;
    }
    public function locality_filter() {
        $filter = false;

		$locality_parents = get_terms('museum_locality', [
			'hide_empty' => true,
			'parent' => 0,
		]);

			// set_transient( 'museum_locality', $museum_locality, 12 * HOUR_IN_SECONDS );
		// }

		if ( 0 < count( $locality_parents ) ) {

			$filter = [];

			foreach ($locality_parents as $p ) {

				$locality_children = get_terms('museum_locality', [
					'hide_empty' => false,
					'parent' => $p->term_id,
				]);

				$filter[$p->term_id]['parent'] = [
					'id' => $p->term_id,
					'slug' => $p->slug,
					'label' => $p->name,
				];

				foreach ($locality_children as $t ) {
					$filter[$p->term_id]['children'][] = [
						'id' => $t->term_id,
						'slug' => $t->slug,
						'label' => $t->name,
					];
				}
			}
		}

        // echo '<pre>'; print_r( $filter ); echo '</pre>';

        return $filter;
    }

    protected function render() {
        $settings = $this->get_settings_for_display();
        
        $selected_type = ( isset( $_GET['type']) && $_GET['type'] !== '' ) ? sanitize_text_field( $_GET['type'] ) : false;
        
        $title_text = ( isset($settings['title']) && $settings['title'] !== '' ) ? $settings['title'] : __('Filter', 'nova');
        $open_btn_text = ( isset($settings['open_button']) && $settings['open_button'] !== '' ) ? $settings['open_button'] : __('Open Filter', 'nova');
        $close_btn_text = ( isset($settings['close_button']) && $settings['close_button'] !== '' ) ? $settings['close_button'] : __('Close Filter', 'nova');
        $clear_btn_text = ( isset($settings['clear_button']) && $settings['clear_button'] !== '' ) ? $settings['clear_button'] : __('Clear Filter', 'nova');
        // echo '<pre>'; print_r( $_GET['type'] ); echo '</pre>';


        // FILTER
        // Område
        // $area = $this->area_filter();
        // Boendetyp
        $type = $this->type_filter();
        // Egenskaper
        $locality = $this->locality_filter();

        echo '<div class="map-filter">';
        printf( 
            '<button data-open-filter class="btn btn-open-filter-dialog" aria-haspopup="true" aria-controls="map-filter-dialog" aria-expanded="false">%s</button>', 
            $open_btn_text
        );
        echo '</div>';

        // DIALOG
        echo '<div data-filter-dialog class="dialog map-filter-dialog" hidden><div class="dialog-contents">';

        echo '<div class="dialog-header">';
        printf( '<h2 class="dialog-title">%s</h2>', $title_text );
        echo '</div>';

        echo '<div class="dialog-content map-filter-content">';
        
        echo '<form class="form">';

        // Area
        if ( is_array( $locality ) ){

			printf( '<fieldset class="fieldset"><legend class="legend">%s</legend>', __( 'Area', 'nova' ) );
            echo '<div class="form-group">';
			echo '<select class="form-input select2" name="area[]" multiple="multiple">';
            foreach ( $locality as $f ) {
				// echo '<pre>'; print_r( $f ); echo '</pre>';
                // printf( 
                //     '<label class="form-label"><input class="form-input filter-area" type="checkbox" value="%s">%s</label>', 
                //     $f['slug'], 
                //     $f['label'] 
                // );
				if ( isset( $f['children'] ) ) {
					printf( 
						'<optgroup label="%s">', 
						$f['parent']['label'] 
					);

					foreach ( $f['children'] as $c ) {

						printf( 
							'<option value="%s">%s</option>', 
							$c['slug'], 
							$c['label'] 
						);
					}

					echo '</optgroup>';
				} else {
					printf( 
						'<option value="%s">%s</option>', 
						$f['parent']['slug'], 
						$f['parent']['label'] 
					);
				}
            }
			echo '</select>';
            echo '</div>';
            echo '</fieldset>';
        }
            
        // Type
        if ( is_array( $type ) && !empty( $type ) ){

            printf( '<fieldset class="fieldset"><legend class="legend">%s</legend>', __( 'Type', 'nova' ) );
            echo '<div class="form-group">';
            foreach ( $type as $f ) {
                printf( 
                    '<label class="form-label"><input class="form-input filter-type" type="checkbox" value="%s" %s>%s</label>', 
                    $f['slug'], 
                    checked( $selected_type, $f['slug'], false ),
                    $f['label']
                );
            }
            echo '</div>';
            echo '</fieldset>';
        }

        // Feature
        // printf( '<fieldset class="fieldset"><legend class="legend">%s</legend>', __( 'Feature', 'nova' ) );
        // echo '<div class="form-group">';
        // foreach ( $locality as $f ) {
        //     printf( '<label class="form-label"><input class="form-input filter-choise" type="checkbox" value="%s">%s</label>', $f['slug'], $f['label'] );
        // }
        // echo '</div>';
        // echo '</fieldset>';
        
        echo '</form></div>';
        
        echo '<div class="dialog-footer map-filter-footer">';
        printf( '<button data-close-filter class="btn btn-close-filter-dialog">%s</button>', $close_btn_text );
        printf( '<button data-clear-filter class="btn btn-clear-filter-dialog">%s</button>', $clear_btn_text );
        echo '</div>';
        echo '</div></div>';

    }

    protected function content_template() {}

}