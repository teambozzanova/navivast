<?php
/**
 * single-gmap.php
 * A Elementor Widget for displaying poi-list
 * 
 * @package		NovaTheme
 * @author		Jon Täng <jon@bozzanova.se>
 * @link	  	https://www.bozzanova.se/
 * @copyright	Bozzanova AB
 */

namespace novatheme\Widgets;

// Elementor
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use WP_Query;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class POI_List extends Widget_Base {

    public function __construct( $data = [], $args = null ) {
        parent::__construct( $data, $args );

        // add_action( 'elementor/editor/after_ register_scripts', [ $this, 'load_editor_scripts' ] );
        // add_action( 'elementor/editor/before_enqueue_scripts', [ $this, 'load_editor_scripts' ] );
        // add_action( 'elementor/editor/before_enqueue_scripts', [ $this, 'editor_scripts' ] );
        // add_action( 'elementor/preview/enqueue_styles', [ $this, 'load_editor_scripts' ] );
        // add_action( 'elementor/frontend/after_register_scripts', array( $this, 'frontend_scripts' ) ); // Register Frontend Scripts

        // add_action( 'elementor/frontend/after_register_scripts', function() {
        //     $json_posts = $this->get_json_posts();
        //     echo '<script id="map-data">';
        //     echo 'window.mapdata = window.mapdata || [];';
        //     printf( 'mapdata = %s;', json_encode( $json_posts, JSON_UNESCAPED_UNICODE ) );
        //     // printf( 'let mapdata = %s;', json_encode( $json_posts, JSON_UNESCAPED_UNICODE ) );
        //     echo '</script>';
        //     // echo '<pre>'; print_r( $poi-list ); echo '</pre>';        
        // }, 10 );
    }

    public function get_name() {
        return 'poi-list';
    }

    public function get_title() {
        return __( 'POI List', 'nova' );
    }

    public function get_icon() {
        return 'eicon-map-pin';
    }

    public function get_custom_help_url() {}

    public function get_categories() {
        return [ 'nova_elements' ];
    }

    public function get_keywords() {
        return [
            __( 'poi-list', 'nova' ), 
            __( 'area listing', 'nova' )
        ];
    }

    public function get_script_depends() {
        
        // wp_add_inline_script( 
        //     'poi-gmap', 
        //     'let mapdata = null', 
        //     'before' 
        // );
        // wp_register_script( 'poi-list', JS_URL . 'poi-list.min.js', [ 'mapbox' ] );
            
        return [
            // 'widget-script'
        ];
    }

    public function get_style_depends() {

        wp_register_style( 'poi-list', CSS_URL . 'poi-list.css' );
            
        return [
            'poi-list'
        ];
    }

    protected function register_controls() {
        
        /*$this->start_controls_section(
            '_content_content',
            [
                'label' => esc_html__( 'Content', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $current_pt = get_post_type();

        if ( 'page' == $current_pt ) {

            $this->add_control(
                'view',
                [
                    'label' => esc_html__( 'View', 'nova' ),
                    'type' => \Elementor\Controls_Manager::HIDDEN,
                    'default' => $current_pt,
                ]
            );

            $this->add_control(
                'post_type',
                [
                    'label' => esc_html__( 'Post Type', 'nova' ),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'default' => 'area',
                    'options' => [
                        'area'  => esc_html__( 'Area', 'nova' ),
                        'property' => esc_html__( 'Property', 'nova' ),
                    ]
                ]
            );

            $terms = get_terms( array(
                'taxonomy' => 'property_type',
                'hide_empty' => false,
            ) );
            $types = [ '0' => '' ];
            foreach ( $terms as $term ) {
                $types[$term->slug] = $term->name;
            }
            
            $this->add_control(
                'terms',
                [
                    'label' => esc_html__( 'Term', 'nova' ),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'default' => '',
                    'options' => $types,
                    'condition' => [
                        'post_type' => 'property',
                    ],
                ]
            );

        }

        if ( 'area' == $current_pt ) {

            $this->add_control(
                'post_type',
                [
                    // 'label' => esc_html__( 'View', 'nova' ),
                    'type' => \Elementor\Controls_Manager::HIDDEN,
                    'default' => 'property',
                ]
            );

        }

        if ( 'property' == $current_pt ) {

            $this->add_control(
                'post_type',
                [
                    // 'label' => esc_html__( 'View', 'nova' ),
                    'type' => \Elementor\Controls_Manager::HIDDEN,
                    'default' => 'area',
                ]
            );

        }

        // echo '<pre>'; print_r( get_post_type() ); echo '</pre>';

        $this->add_control(
			'show_year',
			[
				'label' => esc_html__( 'Show Construction Year', 'nova' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'nova' ),
				'label_off' => esc_html__( 'Hide', 'nova' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->end_controls_section(); */

        // STYLE ----------------------------------------------------

        // List
        $this->start_controls_section(
            '_style_list',
            [
                'label' => esc_html__( 'List', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        // columns (responsive)
        $this->add_responsive_control(
			'list_columns',
			[
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label' => esc_html__( 'Columns', 'nova' ),
				'min' => 1,
				'max' => 8,
				'step' => 1,
				'placeholder' => 3,
				'default' => 3,
				'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => 3,
				'tablet_default' => 2,
				'mobile_default' => 1,
				'selectors' => [
					'{{WRAPPER}} .poi-list' => '--columns: {{SIZE}};'
				],
			]
        );
        // gap
        $this->add_responsive_control(
            'list_gap_col',
			[
                'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Column Space', 'nova' ),
                'size_units' => [ 'px', 'rem', '%' ],
				'range' => [
                    'px' => [
                        'min' => 0,
						'max' => 100,
					],
					'rem' => [
                        'min' => 0,
						'max' => 10,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
                    'size' => 1,
					'unit' => 'rem',
				],
				'default' => [
                    'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
                    '{{WRAPPER}} .poi-list' => 'column-gap: {{SIZE}}{{UNIT}};',
				],
                ]
            );
            
            $this->add_responsive_control(
                'list_gap_row',
                [
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Row Space', 'nova' ),
                'size_units' => [ 'px', 'rem', '%' ],
				'range' => [
                    'px' => [
                        'min' => 0,
						'max' => 100,
					],
					'rem' => [
                        'min' => 0,
						'max' => 10,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
                    'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
                    'size' => 1,
					'unit' => 'rem',
				],
				'default' => [
                    'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
                    '{{WRAPPER}} .poi-list' => 'row-gap: {{SIZE}}{{UNIT}};',
				],
                ]
            );
            
        $this->end_controls_section();

        // Post
        $this->start_controls_section(
            '_style_post',
            [
                'label' => esc_html__( 'Post', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
			'post_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Space', 'nova' ),
                'size_units' => [ 'px', 'rem', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
					'rem' => [
						'min' => 0,
						'max' => 10,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .poi-list .poi' => 'gap: {{SIZE}}{{UNIT}};',
				],
			]
        );
        // background
        $this->add_control(
			'post_bg',
			[
				'label' => esc_html__( 'Background', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .poi-list .poi' => 'background-color: {{VALUE}}',
				],
			]
		);
        // border
        $this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'post_border',
				'label' => esc_html__( 'Border', 'nova' ),
				'selector' => '{{WRAPPER}} .poi-list .poi',
			]
		);
        // raduis
        $this->add_responsive_control(
			'post_radius',
			[
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'label' => esc_html__( 'Border Radius', 'nova' ),
				'size_units' => [ 'px', 'rem', 'em' ],
                'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
                    'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .poi-list .poi' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);
        // box shadow
        $this->add_control(
            'popover-toggle',
            [
                'type' => \Elementor\Controls_Manager::POPOVER_TOGGLE,
                'label' => esc_html__( 'Box Shadow', 'nova' ),
                'label_off' => esc_html__( 'Default', 'nova' ),
                'label_on' => esc_html__( 'Custom', 'nova' ),
                'return_value' => 'yes',
            ]
        );
        
        $this->start_popover();
        
        $this->add_control(
			'post_box_shadow',
			[
				'label' => esc_html__( 'Box Shadow', 'nova' ),
				'type' => \Elementor\Controls_Manager::BOX_SHADOW,
				'selectors' => [
					'{{WRAPPER}} .poi-list .poi' => 'box-shadow: {{HORIZONTAL}}px {{VERTICAL}}px {{BLUR}}px {{SPREAD}}px {{COLOR}};',
				],
			]
		);
        
        $this->end_popover();
        

        $this->end_controls_section();

        // Image
        $this->start_controls_section(
            '_style_image',
            [
                'label' => esc_html__( 'Image', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        // ratio
        $this->add_control(
			'image_ratio',
			[
				'label' => esc_html__( 'Image Ratio', 'nova' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '1 / 1',
				'options' => [
					'1 / 1'  => __( '1 / 1', 'nova' ),
					'3 / 2'  => __( '3 / 2', 'nova' ),
					'4 / 3'  => __( '4 / 3', 'nova' ),
					'5 / 3'  => __( '5 / 3', 'nova' ),
					'16 / 9'  => __( '16 / 9', 'nova' ),
					'3 / 5'  => __( '3 / 5', 'nova' ),
					'3 / 4'  => __( '3 / 4', 'nova' ),
					'2 / 3'  => __( '2 / 3', 'nova' )
				],
                'selectors' => [
					'{{WRAPPER}} .poi-list .poi-image' => '--image-ratio: {{VALUE}};',
				]
			]
		);

        $this->end_controls_section();
        
        // Post Body
        $this->start_controls_section(
            '_style_body',
            [
                'label' => esc_html__( 'Content', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        // padding
        $this->add_responsive_control(
			'body_padding',
			[
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'label' => esc_html__( 'Padding', 'nova' ),
				'size_units' => [ 'px', 'rem', 'em' ],
                'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
                    'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
                'default' => [
                    'top' => 0,
                    'right' => 0,
                    'bottom' => 0,
                    'left' => 0,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .poi-list .poi-body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				]
			]
		);

        // title - type, color
        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Typography', 'nova' ),
				'name' => 'post_type',
				'show_label' => true,
				'label_block' => true,
				'description' => '',
                'default' => '16px',
				// 'separator' => 'default',
				'selector' => '{{WRAPPER}} .poi-list .poi-title'
			]
		);
        $this->add_control(
			'post_color',
			[
				'label' => esc_html__( 'Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .poi-list .poi-title' => 'color: {{VALUE}}',
					'{{WRAPPER}} .poi-list .poi-title a' => 'color: {{VALUE}}'
				]
			]
		);

        $this->end_controls_section();

    }

    public function get_pois() {
        global $post;

        $settings = $this->get_settings_for_display();

        $post_type = ( $settings['post_type'] ) ? $settings['post_type'] : 'area';
        // $filter_term = ( $settings['types'] ) ? $settings['types'] : '';
        $filter_term = ( isset( $settings['terms'] ) && $settings['terms'] != '0' ) ? $settings['terms'] : false;
        $show_year = ( $settings['show_year'] == 'yes' ) ? true : false;
        // delete_transient( 'area_query' );

        // $post_types = ['area', 'property'];

        // if ( false === ( $q = get_transient( 'area_query' ) ) ) {
            // It wasn't there, so regenerate the data and save the transient
            $area_query_args = [
                'post_type' => 'museum',
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'orderby' => [ 'title' => 'ASC' ]
            ];
            
            $q = new WP_Query( $area_query_args );

            // Store query
        //     set_transient( 'area_query', $q, 12 * HOUR_IN_SECONDS );
        // }
        $type = get_post_type();
    
        $posts = [
            
        ];

        if ( $q->have_posts() ) {
            
            while ( $q->have_posts() ) {
                $q->the_post();
                $id = get_the_id(); 
                $title = get_the_title();
                $url = get_permalink();
                $image_id = get_post_thumbnail_id();

                $thumbnail = '';
                if ( has_post_thumbnail( $id ) ) {
                    $thumbnail = get_the_post_thumbnail_url( $id, 'thumbnail' );
                }

                $pos = get_post_meta( $id, 'address', true );
                // $pos = get_field( 'position' );
                
                $p = [
                    'id' => $id,
                    'title' => $title,
                    'url' => $url,
                    'thumbnail' => $thumbnail,
                    'image_id' => $image_id,
                    'lat' => $pos['lat'],
                    'lng' => $pos['lng'],
                ];

                $localities = [];
                if ( $terms = get_the_terms( $id, 'museum_locality' ) ) {
                    foreach ( $terms as $t ) {
                        $localities[] = $t->slug;
                    }

                    $p['locality'] = join(' ', $localities );
                }

                $types = [];
                if ( $terms = get_the_terms( $id, 'museum_type' ) ) {
                    foreach ( $terms as $t ) {
                        $types[] = $t->slug;
                    }

                    $p['types'] = join(' ', $types );
                }

                $posts[] = $p;

            }

        } else {
            $posts = false;
        }

        wp_reset_query();

        return $posts;
    }


    public function render_pois( $pois = [], $options = null ) {

        // echo '<pre>'; print_r( $pois ); echo '</pre>';

        foreach ( $pois as $p )  {
            $class = [ 'poi', 'poi-item' ];

            printf( 
                '<li class="%s" data-link-target="%s" data-id="%s" data-locality="%s" data-types="%s">', 
                join( ' ', $class ), 
                $p['url'],
                $p['id'],
                $p['locality'],
                $p['types']
            );
                
                // Image
                echo '<div class="poi-image">';
                if ( has_post_thumbnail( $p['id'] ) ) {
                    printf( 
                        '%s',
                        get_the_post_thumbnail( $p['id'], 'medium' )
                    );
                }
                echo '</div>';
            
                // Body
                echo '<div class="poi-body">';
                    
                    // Title
                    printf( 
                        '<h2 class="poi-title"><a class="poi-link" href="%s">%s%s</a></h2>',
                        $p['url'],
                        $p['title'],
                        '' // sprintf( '<span class="icon">%s</span>', \novatheme\get_svg_icon( 'angle-right', 24 ) ) 
                    );

            echo '</div>
            </li>';
        } 

    }

    protected function render() {
        $settings = $this->get_settings_for_display();
        // $post_type = ( isset($settings['post_type']) &&  !empty($settings['post_type']) ) ? $settings['post_type'] : 'area';
        
        $pois = $this->get_pois();
        
        echo '<ul class="list poi-list" data-toggle-list>';

        if ( $pois )  {

            $this->render_pois( $pois );

        } 

        echo '</ul>';
    }

    protected function content_template() {}

}