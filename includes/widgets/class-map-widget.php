<?php

namespace novatheme\Widgets;

// Elementor
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Map_Widget extends Widget_Base {

    public function get_name() {
        return 'nova-map';
    }

    public function get_title() {
        return __( 'Google Map', 'nova' );
    }

    public function get_icon() {
        return 'fas fa-th-large';
    }

    public function get_custom_help_url() {}

    public function get_categories() {
        return [ 'nova_elements' ];
    }

    public function get_keywords() {
        return array( 'image', 'box', 'info', 'cta' );
    }

    public function get_script_depends() {
        $gmap_key = get_option( 'options_google_maps_api_key' );
        if ( $gmap_key ) {
			//https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY
			
			wp_register_script( 
				'nova-gmap',  
				'https://maps.googleapis.com/maps/api/js?key=' . $gmap_key, 
				'', //[ 'elementor-frontend' ], 
				null, 
				true 
			);
        }

        wp_register_script( 'nova-map-widget', JS_URL . 'map-widget.js', ['jquery', 'nova-gmap'] );
            
        return [
            'nova-gmap',
            'nova-map-widget'
            // 'widget-script'
        ];
    }

    public function get_style_depends() {

        wp_register_style( 'map-widget', CSS_URL . 'map-widget.css' );
            
        return [
            'map-widget'
        ];
    }

    protected function register_controls() {

        // MAP LAYOUT
        $this->start_controls_section(
            'section_layout',
            [
                'label' => esc_html__( 'Map Layout', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
			'layout_map_height',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Map Height', 'nova' ),
                'size_units' => [ 'px', 'rem', 'vh', '%' ],
				'range' => [
					'vh' => [
						'min' => 0,
						'max' => 100,
					],
                    '%' => [
						'min' => 0,
						'max' => 100,
					],
                    'px' => [
						'min' => 0,
						'max' => 1000,
					],
                    'rem' => [
						'min' => 0,
						'max' => 60,
					],
				],
				'devices' => [ 'widescreen', 'desktop', 'laptop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 400,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 400,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 400,
					'unit' => 'px',
				],
                'default' => [
                    'size' => 400,
					'unit' => 'px',
                ],
				'selectors' => [
					'{{WRAPPER}} .map' => 'min-height: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

        $this->add_control(
			'layout_map_icon_default',
			[
				'type' => \Elementor\Controls_Manager::MEDIA,
				'label' => esc_html__( 'Choose Defualt Icon', 'nova' ),
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				]
			]
		);

        $this->add_control(
			'layout_map_icon_active',
			[
				'type' => \Elementor\Controls_Manager::MEDIA,
				'label' => esc_html__( 'Choose Active Icon', 'nova' ),
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				]
			]
		);

        $this->end_controls_section();

        //  INFOBOX LAYOUT
        $this->start_controls_section(
            'section_infobox',
            [
                'label' => esc_html__( 'Infobox Layout', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
			'infobox_width',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Size', 'nova' ),
				'range' => [
					'px' => [
						'min' => 200,
						'max' => 600,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 600,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 400,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 300,
					'unit' => 'px',
				],
				'selectors' => [
					'{{WRAPPER}} .google-map .infobox' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

        // $this->add_control(
		// 	'infobox_layout_show_image',
		// 	[
		// 		'label' => __( 'Show Image', 'nova' ),
		// 		'type' => \Elementor\Controls_Manager::SWITCHER,
		// 		'label_on' => __( 'Show', 'nova' ),
		// 		'label_off' => __( 'Hide', 'nova' ),
		// 		'return_value' => 'yes',
		// 		'default' => 'yes',
		// 	]
		// );

        // $this->add_control(
		// 	'infobox_layout_show_address',
		// 	[
		// 		'label' => __( 'Show Image', 'nova' ),
		// 		'type' => \Elementor\Controls_Manager::SWITCHER,
		// 		'label_on' => __( 'Show', 'nova' ),
		// 		'label_off' => __( 'Hide', 'nova' ),
		// 		'return_value' => 'yes',
		// 		'default' => 'yes',
		// 	]
		// );

        $this->end_controls_section();

        // STYLE
        // $this->start_controls_section(
        //     'section_style',
        //     [
        //         'label' => esc_html__( 'Style', 'nova' ),
        //         'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        //     ]
        // );

        // $this->add_control(
        //     'color',
        //     [
        //         'label' => esc_html__( 'Color', 'nova' ),
        //         'type' => \Elementor\Controls_Manager::COLOR,
        //         'default' => '#f00',
        //         'selectors' => [
        //             '{{WRAPPER}} h3' => 'color: {{VALUE}}',
        //         ],
        //     ]
        // );  

        $this->end_controls_section();

    }

    public function create_posts( $settings ) {
        
        $post_type = 'museum';
        // $taxonomy = 'event_category';
        // $terms = ( $settings['terms'] ) ?: false;
        $posts_per_page = ( $settings['posts_per_page'] ) ?: -1;
        // $orderby = ( $settings['orderby'] ) ?: [ 'date' => 'DESC' ];

        // echo '<pre>'; print_r( $settings ); echo '</pre>';

        $icon_default = $settings['layout_map_icon']['url'];
        $icon_active = $settings['layout_map_icon']['url'];

        $query_args = [
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page
        ];

        if ( 0 != $terms ) {
            $query_args = array_merge(
                $query_args, [
                    'tax_query' => [
                        [
                            'taxonomy' => $taxonomy,
                            'field'    => 'id',
                            'terms'    => $terms,
                        ],
                    ],
                    'orderby' => $orderby
                ]
            );
        }

        $q = new \WP_Query( $query_args );

        if ( ! $q->have_posts() ) 
            return false;

        $posts = [];

        foreach ( $q->posts as $p ) {
            $loc = get_field( 'address', $p->ID );
            if ( $default = get_field( 'google_maps_icon_default', 'option' ) ) {
                // echo '<pre>'; print_r( $image ); echo '</pre>';
                $icon_default = $default['sizes']['marker'];
            }
            if ( $active = get_field( 'google_maps_icon_active', 'option' ) ) {
                // echo '<pre>'; print_r( $image ); echo '</pre>';
                $icon_active = $active['sizes']['marker'];
            }

            $image = false;
            if ( ( has_post_thumbnail( $p->ID ) ) && ( 'yes' === $settings['infobox_layout_show_image'] ) ) {
                $image = get_the_post_thumbnail_url( $p->ID, 'thumbnail' );
            }

            $type = get_the_terms( $id, 'museum_type' );
            $type = wp_list_pluck( $type, 'slug' );
            $locality = get_the_terms( $id, 'museum_locality' );
            $locality = wp_list_pluck( $locality, 'slug' );

            $post = [
                'id' => $p->ID,
                'name' => $p->post_title,
                'link' => get_permalink( $p->ID ),
                // 'address' => get_field( 'place_address', $p->ID ),
                // 'website' => get_field( 'place_website', $p->ID ),
                'lat' => (float) $loc['lat'],
                'lng' => (float) $loc['lng'],
                'loc' => $loc,
                'icon_default' => $icon_default,
                'icon_active' => $icon_active,
                'image' => false,
                'type' => $type,
                'locality' => $locality,
            ];

            $posts[] = $post;
            
        }

        wp_reset_query();

        return $posts;
    }

    protected function render() {
        $gmap_key = get_option( 'options_google_maps_api_key' );
        // echo '<pre>'; print_r( $gmap_key ); echo '</pre>';

        $settings = $this->get_settings_for_display();

        $classes = ['google-map'];
        if ('yes' === $settings['infobox_layout_show_image']) {
            $classes[] = 'show-image';
        }
        
        $posts = $this->create_posts( $settings );

        if ( ! is_array( $posts ) ) return;

        // echo '<div class="google-map">
        //     <div id="map" class="map">Google Map</div>
        //     <div id="infobox" class="infobox"><button id="close" class="close"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" class="svg-inline--fa fa-times fa-w-11 fa-5x"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" class=""></path></svg><span class="screen-reader-text">'. __( 'Close', 'nova' ). '</span></button></div></div>';
        printf( 
            '<div class="%s">
                <div id="map" class="map"></div>
                <div id="infobox" class="infobox">
                <button id="close" class="close"><svg aria-hidden="true" focusable="false" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" class=""><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" class=""></path></svg><span class="screen-reader-text">%s</span></button>
                </div>
            </div>',
            join( ' ', $classes ),
            __( 'Close', 'nova' )
        );

        printf( 
			'<script>let pois = %s</script>',
			json_encode( $posts )
		);

        //  echo '<pre>'; print_r( $posts ); echo '</pre>';

    }

    protected function content_template() {}

}