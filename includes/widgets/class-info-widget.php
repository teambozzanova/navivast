<?php

namespace novatheme\Widgets;

// Elementor
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Icons_Manager;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Info_Widget extends Widget_Base {

    public function get_name() {
        return 'nova-info';
    }

    public function get_title() {
        return __( 'Museum Information', 'nova' );
    }

    public function get_icon() {
        return 'fas fa-th-large';
    }

    public function get_custom_help_url() {}

    public function get_categories() {
        return [ 'nova_elements' ];
    }

    public function get_keywords() {
        return array( 'image', 'box', 'info', 'cta' );
    }

    public function get_script_depends() {
        // wp_register_script( 'widget-script', JS_URL . 'widget-script.js' );
            
        return [
            // 'widget-script'
        ];
    }

    public function get_style_depends() {

        wp_register_style( 'museum-info', CSS_URL . 'museum-info-widget.css' );
            
        return [
            'museum-info'
        ];
    }

    protected function register_controls() {

        //  CONTENT
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Content', 'nova' ),
            ]
        );

        $this->end_controls_section();

        // STYLE
        // $this->start_controls_section(
        //     'section_style',
        //     [
        //         'label' => esc_html__( 'Style', 'nova' ),
        //         'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        //     ]
        // );
        // $this->end_controls_section();

        // BLOCK STYLE
		$this->start_controls_section(
			'section_block_style',
			[
				'label' => esc_html__( 'Block Style', 'nova' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

        $this->add_responsive_control(
			'block_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Gap', 'nova' ),
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .museum-information ul' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

        $this->end_controls_section();

    }

    
    protected function render() {
        global $post;

        $settings = $this->get_settings_for_display();
        // \Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );

        echo '<div class="museum-information">';

        echo '<ul>';

        if ( $address = get_field( 'address_string' ) ) {
            printf( 
                // '<li class="address"><i class="fa-solid fas fa-house"></i> %s</li>', 
                '<li class="address">%s</li>', 
                str_replace( ', ', '<br>', $address )
            );
        }

        if ( $phone = get_field( 'phone' ) ) {
            printf( 
                // '<li class="phone"><i class="fa-solid fas fa-phone"></i> <a href="tel:%s">%s</a></li>', 
                '<li class="phone"><a href="tel:%s">%s</a></li>', 
                $phone,
                $phone
            );
        }

        if ( $phone_2 = get_field( 'phone_2' ) ) {
            printf( 
                // '<li class="phone"><i class="fa-solid fas fa-phone"></i> <a href="tel:%s">%s</a></li>', 
                '<li class="phone"><a href="tel:%s">%s</a></li>', 
                $phone_2,
                $phone_2
            );
        }

        if ( $mail = get_field( 'mail' ) ) {
            printf( 
                // '<li class="email"><i class="fa-solid fas fa-envelope"></i> <a href="mailto:%s">%s</a></li>', 
                '<li class="email"><a href="mailto:%s">%s</a></li>', 
                antispambot( $mail, 1 ),
                antispambot( $mail )
            );
        }

        if ( $url = get_field( 'website' ) ) {
           
            printf( 
                // '<li class="website"><i class="fa-solid fas fa-globe"></i> <a href="%s" target="_blank">%s</a></li>', 
                '<li class="website"><a href="%s" target="_blank">%s</a></li>', 
                esc_url( $url ),
                preg_replace( "#^[^:/.]*[:/]+#i", '', $url ) //__( 'Website', 'nova' )
            );
        }

        if ( $location = get_field( 'address' ) ) {
            $lat = $location['lat'];
            $lng = $location['lng'];
            $url = sprintf( "https://www.google.com/maps/dir/current+location/%s,%s/", $lat, $lng ); 
           
            printf( 
                // '<li class="directions"><i class="fa-solid fas fa-map-sign"></i> <a href="%s" target="_blank">%s</a></li>', 
                '<li class="directions"><a href="%s" target="_blank">%s</a></li>', 
                esc_url( $url ),
                __( 'Directions', 'nova' )
            );
        }
        
        echo '</ul>';
        echo '</div>';
    }

    protected function content_template() {}
}