<?php

namespace novatheme\Widgets;

// Elementor
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Icons_Manager;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Navigation_Widget extends Widget_Base {

    public function get_name() {
        return 'nova-navigation';
    }

    public function get_title() {
        return __( 'Nav Menu', 'nova' );
    }

    public function get_icon() {
        return 'fas fa-th-large';
    }

    public function get_custom_help_url() {}

    public function get_categories() {
        return [ 'nova_elements' ];
    }

    public function get_keywords() {
        return array( 'menu', 'nav', 'navigation' );
    }

    public function get_script_depends() {
        // wp_register_script( 'widget-script', JS_URL . 'widget-script.js' );
            
        return [
            // 'widget-script'
        ];
    }

    public function get_style_depends() {

        wp_register_style( 'navigation', CSS_URL . 'navigation-widget.css' );
            
        return [
            'navigation'
        ];
    }

    protected function register_controls() {

        //  CONTENT
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Content', 'nova' ),
            ]
        );

        $options = [];
        $menus = get_terms( [ 
            'taxonomy' => 'nav_menu',
            'hide_empty' => false,
        ] );

        foreach ( $menus as $m ) {
            $options[ $m->term_id ] = $m->name;
        }


        $this->add_control(
			'menu',
			[
				'label' => esc_html__( 'Chose Menu', 'nova' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => false,
				'options' => $options,
				'default' => [],
			]
		);

        $this->end_controls_section();

        

        // STYLE
        // $this->start_controls_section(
        //     'section_style',
        //     [
        //         'label' => esc_html__( 'Style', 'nova' ),
        //         'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        //     ]
        // );
        // $this->end_controls_section();

        // BLOCK STYLE
		$this->start_controls_section(
			'section_block_style',
			[
				'label' => esc_html__( 'Block Style', 'nova' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

        $this->add_responsive_control(
			'block_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Gap', 'nova' ),
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .museum-information ul' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

        $this->end_controls_section();

    }

    
    protected function render() {
        global $post;

        $settings = $this->get_settings_for_display();

        // echo '<pre>'; print_r( $menus ); echo '</pre>';

        $nav_args = array(
            'menu'                 => $settings['menu'],
            'container'            => 'nav',
            // 'container_class'      => '',
            // 'container_id'         => '',
            // 'container_aria_label' => '',
            // 'menu_class'           => 'menu',
            // 'menu_id'              => '',
            // 'echo'                 => true,
            // 'fallback_cb'          => 'wp_page_menu',
            // 'before'               => '',
            // 'after'                => '',
            // 'link_before'          => '',
            // 'link_after'           => '',
            'items_wrap'           => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            // 'item_spacing'         => 'preserve',
            // 'depth'                => 0,
            // 'walker'               => '',
            // 'theme_location'       => '',
        );
        
        wp_nav_menu( $nav_args );
        
    }

    protected function content_template() {}

}