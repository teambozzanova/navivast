<?php

namespace novatheme\Widgets;

// Elementor
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Facilities_Widget extends Widget_Base {

    public function get_name() {
        return 'nova-facilities';
    }

    public function get_title() {
        return __( 'Museum Facilities', 'nova' );
    }

    public function get_icon() {
        return 'fas fa-th-large';
    }

    public function get_custom_help_url() {}

    public function get_categories() {
        return [ 'nova_elements' ];
    }

    public function get_keywords() {
        return array( 'image', 'box', 'info', 'cta' );
    }

    public function get_script_depends() {
        // wp_register_script( 'widget-script', JS_URL . 'widget-script.js' );
            
        return [
            // 'widget-script'
        ];
    }

    public function get_style_depends() {

        wp_register_style( 'museum-facilities', CSS_URL . 'museum-facilities-widget.css' );
            
        return [
            'museum-facilities'
        ];
    }

    protected function register_controls() {

        //  CONTENT
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Content', 'nova' ),
            ]
        );

        $this->end_controls_section();

         // BLOCK STYLE
		$this->start_controls_section(
			'section_block_style',
			[
				'label' => esc_html__( 'Block Style', 'nova' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

        $this->add_responsive_control(
			'block_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Gap', 'nova' ),
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .facilities ul' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

        $this->add_responsive_control(
			'block_cols',
			[
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label' => esc_html__( 'Columns', 'nova' ),
				'placeholder' => '0',
				'min' => 0,
				'max' => 4,
				'step' => 1,
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'desktop_default' => 3, 
                'tablet_default' => 2,
                'mobile_default' => 1,             
                'selectors' => [
					'{{WRAPPER}} .facilities ul' => 'grid-template-columns: repeat({{SIZE}}, 1fr);'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
                // 'dynamic' => [
                //     'active' => true,
                // ],
			]
		);

        $this->end_controls_section();

        // BOX STYLE
		$this->start_controls_section(
			'section_box_style',
			[
				'label' => esc_html__( 'Box Style', 'nova' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

        $this->add_responsive_control(
			'box_padding',
			[
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'label' => esc_html__( 'Padding', 'nova' ),
				'size_units' => [ 'px', 'rem', 'em' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
                    'top' => 0.5,
                    'right' => 1,
                    'bottom' => 0.5,
                    'left' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'top' => 0.5,
                    'right' => 1,
                    'bottom' => 0.5,
                    'left' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'top' => 0.5,
                    'right' => 1,
                    'bottom' => 0.5,
                    'left' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .facility' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
                'dynamic' => [
                    'active' => true,
                ],
			]
		);

        $this->add_responsive_control(
			'box_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Gap', 'nova' ),
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .facility' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

		$this->add_control(
			'box_background_color',
			[
				'label' => esc_html__( 'Box Background Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#eee',
				'selectors' => [
					'{{WRAPPER}} .facility' => 'background-color: {{VALUE}}',
				],
			]
		);

        $this->end_controls_section();

        // ICON STYLE
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon Style', 'nova' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

        $this->add_responsive_control(
			'icon_size',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Icon Size', 'nova' ),
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 3,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 3,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 2,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .facility-icon' => 'inline-size: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

        $this->end_controls_section();

        // TEXT STYLE
        $this->start_controls_section(
			'section_box_content',
			[
				'label' => esc_html__( 'Box Content', 'nova' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

        $this->add_control(
			'text_color',
			[
				'label' => esc_html__( 'Text Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .facility-info' => 'color: {{VALUE}}',
				],
			]
		);

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Label', 'nova' ),
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .facility-label',
			]
		);

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Description', 'nova' ),
				'name' => 'description_typography',
				'selector' => '{{WRAPPER}} .facility-description',
			]
		);

        $this->end_controls_section();

    }


    protected function render() {
        global $post;

        $settings = $this->get_settings_for_display();
        // echo '<pre>'; print_r( $settings ); echo '</pre>';

        $info = [
            'museiresan' => [
                'img' => 'museiresan.png',
                'label' => __( 'Museiresan', 'nova' ),
                'desc' => __( 'Detta museum är med i Museiresan', 'nova' )
            ],
            'cafe' => [
                'img' => 'cafe_xl.png',
                'label' => __( 'Kaffe', 'nova' ),
                'desc' => __( 'Enklare servering (kaffe, läsk, kaka) vid ordinarie öppettider.', 'nova' )
            ],
            'cafe_xl' => [
                'img' => 'cafe.png',
                'label' => __( 'Servering', 'nova' ),
                'desc' => __( 'Servering av mat (smörgås, paj) vid ordinarie öppettider.', 'nova' )
            ],
            'sale' => [
                'img' => 'sale.png',
                'label' => __( 'Försäljning', 'nova' ),
                'desc' => __( 'Produkter och souvenirer.', 'nova' )
            ],
            'parking' => [
                'img' => 'parking.png',
                'label' => __( 'Parkering', 'nova' ),
                'desc' => __( 'Finns i anslutning till besöksmålet.', 'nova' )
            ],
            'accessibility' => [
                'img' => 'accessibility.png',
                'label' => __( 'Tillgänglighet', 'nova' ),
                'desc' => __( 'Finns beskriven på egen hemsida eller anslutna till Tillgänglighetsdatabasen (TD).', 'nova' )
            ],
            'wheelchair' => [
                'img' => 'wheelchair.png',
                'label' => __( 'Handikappvänligt', 'nova' ),
                'desc' => __( 'Museet är anpassat för rullstol.', 'nova' )
            ],
            'wc' => [
                'img' => 'wc.png',
                'label' => __( 'Toalett', 'nova' ),
                'desc' => __( 'Finns i anslutning till besöksmålet.', 'nova' )
            ],
            'hwc' => [
                'img' => 'hwc.png',
                'label' => __( 'Handikapptoalett', 'nova' ),
                'desc' => __( 'Anpassat för rullstol finns i anslutning till besöksmålet.', 'nova' )
            ],
            'privy' => [
                'img' => 'privy.png',
                'label' => __( 'Utmärkt torrdass', 'nova' ),
                'desc' => __( 'Finns i anslutning till besöksmålet.', 'nova' )
            ],
            'h_privy' => [
                'img' => 'h_privy.png',
                'label' => __( 'Handikapp torrdass', 'nova' ),
                'desc' => __( 'Anpassat för rullstol finns i anslutning till besöksmålet.', 'nova' )
            ],
            'business_hours' => [
                'img' => 'business-hours.png',
                'label' => __( 'Öppettider', 'nova' ),
                'desc' => __( 'Information finns på egen hemsida.', 'nova' )
            ],
            'booking_only' => [
                'img' => 'pre_booking.png',
                'label' => __( 'Öppet', 'nova' ),
                'desc' => __( 'Efter överenskommelse', 'nova' )
            ]

        ];

        echo '<div class="facilities">';
        echo '<ul>';

        foreach ( $info as $k => $v ) {

            // echo '<pre>'; print_r( $k ); echo '</pre>';
            
            if ( $data = get_field( $k ) ) {
                printf( 
                    '<li class="facility %s">
                        <img class="facility-icon" src="%s" alt="icon">
                        <div class="facility-info">
                            <div class="facility-label">%s</div>
                            <div class="facility-description">%s</div>
                        </div>
                    </li>', 
                    $k,
                    IMG_URL .'icons/' . $v['img'],
                    ( $v['label'] ) ?: '',
                    ( $v['desc'] ) ?: ''
                );
            }
        }

        echo '</ul>';
        echo '</div>';
    }



}