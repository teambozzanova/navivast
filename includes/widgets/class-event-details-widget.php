<?php

namespace novatheme\Widgets;

// Elementor
use Elementor\Widget_Base;
use Elementor\Utils;
use Elementor\Control_Media;
use Elementor\Controls_Manager;
use Elementor\Control_Image_Dimensions;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use WP_Query;
use Date;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Event_Details_Widget extends Widget_Base {

    public function get_name() {
        return 'nova-event-details';
    }

    public function get_title() {
        return __( 'Event Details', 'nova' );
    }

    public function get_icon() {
        return 'fas fa-th-large';
    }

    public function get_custom_help_url() {}

    public function get_categories() {
        return [ 'nova_elements' ];
    }

    public function get_keywords() {
        return array( 'event', 'evenemang', 'detaljer' );
    }

    public function get_script_depends() {
        // wp_register_script( 'widget-script', JS_URL . 'widget-script.js' );
            
        return [
            // 'widget-script'
        ];
    }

    public function get_style_depends() {

        wp_register_style( 'event-details-widget', CSS_URL . 'event-details-widget.css' );
            
        return [
            'event-details-widget'
        ];
    }

    protected function register_controls() {

        //  QUERY
        /*
		$this->start_controls_section(
            'content_query_section',
            [
                'label' => esc_html__( 'Query', 'nova' ),
            ]
        );

        $this->add_control(
			'query_orderby',
			[
				'type' => \Elementor\Controls_Manager::SELECT,
				'label' => esc_html__( 'Orderby', 'nova' ),
				'options' => [
					// 'default' => esc_html__( 'Default', 'nova' ),
					'event_date' => esc_html__( 'Event Date', 'nova' ),
					'post_date' => esc_html__( 'Post Date', 'nova' ),
					'title' => esc_html__( 'Title', 'nova' ),
				],
				'default' => 'event_date',
			]
		);

        $this->add_control(
			'query_order',
			[
				'type' => \Elementor\Controls_Manager::SELECT,
				'label' => esc_html__( 'Orderby', 'nova' ),
				'options' => [
					'asc' => esc_html__( 'ASC', 'nova' ),
					'desc' => esc_html__( 'DESC', 'nova' ),
				],
				'default' => 'desc',
			]
		);

        $this->end_controls_section(); 
		*/

        //  LAYOUT
        $this->start_controls_section(
            'content_layout_section',
            [
                'label' => esc_html__( 'Layout', 'nova' ),
            ]
        );

        $this->add_responsive_control(
			'block_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Gap', 'nova' ),
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .event-meta' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

		$this->add_control(
			'layout_date_format',
			[
				'label' => esc_html__( 'Date Format', 'nova' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'Y-m-d',
				'options' => [
					'Y-m-d'  => esc_html__( '2021-12-31', 'nova' ),
					'j F, Y' => esc_html__( '31 december, 2021', 'nova' ),
					
				],
				// 'selectors' => [
				// 	'{{WRAPPER}} .event-post-image img' => '--image-ratio: {{OPTION}};'
				// ],
				// 'condition' => [
                //     'layout_show_date' => 'yes',
                // ],
			]
		);

		/*
        $this->add_control(
			'layout_show_image',
			[
				'label' => __( 'Show Image', 'nova' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'nova' ),
				'label_off' => __( 'Hide', 'nova' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->add_group_control(
            \Elementor\Group_Control_Image_Size::get_type(),
            [
                'name' => 'image', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `thumbnail_size` and `thumbnail_custom_dimension`.
                'exclude' => ['custom'],
                'include' => [],
                'default' => 'map_icon',
                'condition' => [
                    'layout_show_image' => 'yes',
                ],
            ]
        );

        $this->add_control(
			'image_width',
			[
				'label' => esc_html__( 'Width', 'nova' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ '%' ],
				'range' => [
					'%' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'default' => [
					'unit' => '%',
					'size' => 50,
				],
				'selectors' => [
					'{{WRAPPER}} .event-post-image' => '--image-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .event-post-body' => '--body-size: calc(100% - {{SIZE}}{{UNIT}});',
				],
				'condition' => [
                    'layout_show_image' => 'yes',
                ],
			]
		);

		$this->add_control(
			'image_style',
			[
				'label' => esc_html__( 'Image Style', 'nova' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '1:1',
				'options' => [
					'1'  => esc_html__( '1:1', 'nova' ),
					'4 / 3' => esc_html__( '4:3', 'nova' ),
					'16 / 9' => esc_html__( '16:9', 'nova' )
				],
				'selectors' => [
					'{{WRAPPER}} .event-post-image' => '--image-ratio: {{OPTION}};'
				],
				'condition' => [
                    'layout_show_image' => 'yes',
                ],
			]
		);


        $this->add_responsive_control(
			'post_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Post Content Gap', 'nova' ),
                'condition' => [
                    'layout_show_image' => 'yes',
                ],
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .event-post' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);

        $this->add_control(
			'layout_show_title',
			[
				'label' => __( 'Show Title', 'nova' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'nova' ),
				'label_off' => __( 'Hide', 'nova' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->add_control(
			'layout_title_size',
			[
				'label' => __( 'Title HTML Tag', 'nova' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'h1' => 'H1',
					'h2' => 'H2',
					'h3' => 'H3',
					'h4' => 'H4',
					'h5' => 'H5',
					'h6' => 'H6',
				],
				'default' => 'h3',
                'condition' => [
                    'layout_show_title' => 'yes'
                ]
			]
		);

        $this->add_control(
			'layout_show_date',
			[
				'label' => __( 'Show Date', 'nova' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'nova' ),
				'label_off' => __( 'Hide', 'nova' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->add_control(
			'layout_show_content',
			[
				'label' => __( 'Show Content', 'nova' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'nova' ),
				'label_off' => __( 'Hide', 'nova' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->add_control(
			'layout_wordcount',
			[
				'type' => \Elementor\Controls_Manager::TEXT,
				'label' => esc_html__( 'Content Word Lenght', 'nova' ),
				// 'placeholder' => esc_html__( 'Enter your title', 'nova' ),
                'condition' => [
                    'layout_show_content' => 'yes',
                ],
			]
		);

        $this->add_responsive_control(
			'post_content_gap',
			[
				'type' => \Elementor\Controls_Manager::SLIDER,
				'label' => esc_html__( 'Image Content Gap', 'nova' ),
                'condition' => [
                    'layout_show_title' => 'yes',
                    'layout_show_content' => 'yes',
                ],
                'size_units' => [ 'px', 'rem', 'em' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices' => [ 'desktop', 'tablet', 'mobile' ],
				'desktop_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'tablet_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'mobile_default' => [
					'size' => 1,
					'unit' => 'rem',
				],
				'selectors' => [
					'{{WRAPPER}} .event-post-body' => 'gap: {{SIZE}}{{UNIT}};'
                    // '{{WRAPPER}} .btn-pause svg' => 'block-size: calc( {{SIZE}}{{UNIT}} - 1rem);',
				],
			]
		);
		*/
        $this->end_controls_section();

        // STYLE
        $this->start_controls_section(
            'section_style',
            [
                'label' => esc_html__( 'Style', 'nova' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        /*$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Title Typography', 'nova' ),
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .event-post-title',
			]
		);

        $this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Title Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .event-post-title a' => 'color: {{VALUE}}',
				],
			]
		);

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Meta Typography', 'nova' ),
				'name' => 'meta_typography',
				'selector' => '{{WRAPPER}} .event-post-meta',
			]
		);

        $this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Meta Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .event-post-meta' => 'color: {{VALUE}}',
				],
			]
		); 
		*/

        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Text Typography', 'nova' ),
				'name' => 'content_typography',
				'selector' => '{{WRAPPER}} .event-meta',
			]
		);

        $this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Text Color', 'nova' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .event-meta' => 'color: {{VALUE}}',
				],
			]
		);
		/*
        $this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
                'label' => esc_html__( 'Label', 'nova' ),
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .facility-label',
			]
		);
*/
        $this->end_controls_section();

    }

    protected function render() {
		// global $post;
        $today = date( 'Ymd' );
        $settings = $this->get_settings_for_display();

		$dateformat = ( $settings['layout_date_format'] ) ?: 'Y-m-d';

		$start_date = get_field('start_date') ?: false;
		$end_date = get_field('end_date') ?: $start_date;
		
		$start_time = get_field('start_time') ?: false;
		$end_time = get_field('end_time') ?: false;

		// echo '<pre>'; print_r( $start_date ); echo '</pre>';
		// echo '<pre>'; print_r( $end_date ); echo '</pre>';
		// echo '<pre>'; print_r( $time ); echo '</pre>';

		// Format Start Date
		if ( $start_date <= $today ) {
			$start_date = __('Ongoing', 'nova');

		} else {
			$start_date = date_i18n( $dateformat, strtotime( $start_date) );	

		}
		
		// Format End Date
		if ( $end_date == $today ) {
			$start_date = __('Ends today', 'nova');
			$end_date = false;
		} elseif ( $end_date < $today ) {
			$start_date = __('Has ended', 'nova');
			$end_date = false;

		} else {
			$end_date = date_i18n( $dateformat, strtotime( $end_date ) );
		}

		echo '<div class="event-meta">';

		// Date Output
		if ( $start_date ) {
			printf( 
				'<div class="event-date"><span class="label">%s:</span> <span class="value">%s%s</span></div>', 
				__( 'Date', 'nova' ),
				$start_date,
				sprintf( '%s',
					( $end_date && ($start_date !== $end_date) ) ? ' - ' . $end_date : ''
				)
			);
		}

		// Time Output
		if ( $start_time ) {
			printf( 
				'<div class="event-time"><span class="label">%s:</span> <span class="value">%s%s</span></div>', 
				__( 'Time', 'nova' ),
				$start_time,
				sprintf( '%s',
					( $end_time ) ? ' - ' . $end_time : ''
				)
			);
		}

		echo '</div>';
        
    }

	protected function content_template() {}

}