<?php
/**
 * @package   Novastyle 2020
 * @author    Jon Täng <jon@bozzanova.se>
 * @link      http://www.bozzanova.se
 * @copyright 2020 Bozzanova AB
 */

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Save ACF JSON
 */
class ACF {
	
	private static $instance = null;

	private $acf_blocks = [];

	/**
	 * Instance function
	 *
	 * @return Theme
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	/**
	 * Constructor
	 */
	function __construct() {
		// ACF Save & Load paths
		add_filter( 'acf/settings/save_json', [ $this, 'acf_json_save_point' ] );
		add_filter( 'acf/settings/load_json', [ $this, 'acf_json_load_point' ] );

		// Option Pages
		add_action( 'acf/init', [ $this, 'acf_option_pages' ] );

		// Google Maps API Key
		add_action( 'acf/init', [ $this, 'acf_maps_key' ] );

		// Load field values
		add_filter('acf/load_value/name=place_google_maps_url', [ $this, 'load_google_maps_url' ], 10, 3 );

	}
	
	/**
	 * Save ACF JSON
	 */
	function acf_json_save_point( $path ) {
		// update path
		$path = ASSET_PATH . '/acf-json';
		// return
		return $path;
	}
	
	/**
	 * Load ACF JSON
	 */
	function acf_json_load_point( $paths ) {
		// remove original path (optional)
		unset( $paths[0] );
		// append path
		$paths[] = ASSET_PATH . '/acf-json';
		// return
		return $paths;
	}
	
	/**
	 * Create ACF Option Pages
	 */
	function acf_option_pages() {	

		if ( function_exists('acf_add_options_page') ) {

			$option_page = acf_add_options_page( 
				array(
					'page_title' => __('Website Settings', 'nova'),
					'menu_title' => __('Website', 'nova'),
					'menu_slug' => 'site-settings',
					'capability' => 'customize',
					//'position' => false,
					//'parent_slug' => '',
					'icon_url' => 'dashicons-admin-tools',
					//'redirect' => true,
					//'post_id' => 'options',
					//'autoload' => false,
					'update_button'		=> __('Save settings', 'nova'),
					'updated_message'	=> __("Settings saved", 'nova'),
				)
			);
			
			$option_page = acf_add_options_page( 
				array(
					'page_title' => __('Company Information', 'nova'),
					'menu_title' => __('Company', 'nova'),
					'menu_slug' => 'company-settings',
					'capability' => 'edit_pages',
					//'position' => false,
					//'parent_slug' => '',
					'icon_url' => 'dashicons-info',
					//'redirect' => true,
					//'post_id' => 'options',
					//'autoload' => false,
					'update_button'		=> __('Save information', 'nova'),
					'updated_message'	=> __("Information saved", 'nova'),
				)
			);

		}
	}
	
	/**
	 * Set Google Maps Key for ACF
	 */
	function acf_maps_key() {
		acf_update_setting( 'google_api_key', get_option( 'options_google_maps_api_key', true ) );
	}

	/**
	 * Load Google Maps URL in text feild
	 * 
	 * @param string $value
	 * @param int $post_id
	 * @param array $field
	 * 
	 * @return string $value
	 */
	public function load_google_maps_url( $value, $post_id, $field ) {
		// global $post;
		// https://www.google.com/maps/@?api=1&map_action=map&parameters

		$url = 'https://www.google.com/maps/dir/?api=1&destination=';

		$place = get_field( 'place_location', $post_id );

		$params = str_replace( ',', '', $place['address'] );
		$params = str_replace( ' ', '%20', $params );

		$value = $url . $params;
		// echo '<pre>'; print_r( $params ); echo '</pre>';

		return $value;
	}

}