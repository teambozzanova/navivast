<?php
/**
 * @package   Novastyle 2020
 * @author    Jon Täng <jon@bozzanova.se>
 * @link      http://www.bozzanova.se
 * @copyright 2020 Bozzanova AB
 */

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Place Post Type Setup
 */
class Museum {

  private static $instance = null;

  public $post_type = null;
  public $post_type_rewrite = null;
  
  public $singular = null;
  public $plural = null;
  
  public $category = null;
  public $singular_cat = null;
  public $plural_cat = null;

  public $tag = null;
  public $singular_tag = null;
  public $plural_tag = null;
  
  public $archive_page = null;
  public $archive_page_slug = null;

	
	/**
	 * Instance function
	 *
	 * @return $instance
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

  /**
   * Class Construct
   *
   * @return void
   */
  public function __construct() {
    add_action( 'init', function () {
      $this->post_type = 'museum';
      $this->post_type_rewrite = _x( 'museum', 'post type rewrite', 'nova' );
      $this->singular = _x( 'Museum', 'Post Type Singular Name', 'nova' );
      $this->plural = _x( 'Museums', 'Post Type Plural Name', 'nova' );
    }, 5 );
    
    // Museum Categories & Tags
    add_action( 'init', [ $this, 'create_post_type' ], 15 );
    add_action( 'init', [ $this, 'create_taxonomies' ], 15 );

    add_action( 'init', [ $this, 'fix_thumbnail' ] );
    add_action( 'init', [ $this, 'fix_gallery' ] );
    add_action( 'init', [ $this, 'fix_content' ] );
    
  }

  /**
   * Create Post Type
   */
  public function create_post_type() {
  
    $labels = array(
      'name'                  => $this->plural,
      'singular_name'         => $this->singular,
      'menu_name'             => $this->plural,
      'name_admin_bar'        => $this->plural,
      'archives'              => __( 'Archives', 'nova' ),
      'attributes'            => __( 'Attributes', 'nova' ),
      'parent_item_colon'     => __( 'Parent item:', 'nova' ),
      'all_items'             => __( 'All item', 'nova' ),
      'add_new_item'          => __( 'Add New item', 'nova' ),
      'add_new'               => __( 'Add New', 'nova' ),
      'new_item'              => __( 'New item', 'nova' ),
      'edit_item'             => __( 'Edit item', 'nova' ),
      'update_item'           => __( 'Update item', 'nova' ),
      'view_item'             => __( 'View item', 'nova' ),
      'view_items'            => __( 'View items', 'nova' ),
      'search_items'          => __( 'Search items', 'nova' ),
      'not_found'             => __( 'Not found', 'nova' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'nova' ),
      'featured_image'        => __( 'Featured Image', 'nova' ),
      'set_featured_image'    => __( 'Set featured image', 'nova' ),
      'remove_featured_image' => __( 'Remove featured image', 'nova' ),
      'use_featured_image'    => __( 'Use as featured image', 'nova' ),
      'insert_into_item'      => __( 'Insert into item', 'nova' ),
      'uploaded_to_this_item' => __( 'Uploaded to this item', 'nova' ),
      'items_list'            => __( 'Items list', 'nova' ),
      'items_list_navigation' => __( 'Itemes list navigation', 'nova' ),
      'filter_items_list'     => __( 'Filter items list', 'nova' ),
    );
    $rewrite = array(
      // 'slug'                  => urldecode( get_page_uri( $this->archive_page ) ) ?: $this->archive_page_slug,
      'slug'                  => $this->post_type_rewrite,
      'with_front'            => true,
      'pages'                 => true,
      'feeds'                 => false,
    );
    $args = array(
      'label'                 => $this->singular,
      // 'description'           => __( 'Post Type for Place Articles', 'nova' ),
      'labels'                => $labels,
      'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'author' ),
      'taxonomies'            => array( $this->category, $this->tag ),
      'hierarchical'          => false,
      'public'                => true,
      // 'show_ui'         7      => true,
      // 'show_in_menu'          => true,
      'menu_position'         => 5,
      'menu_icon'             => 'dashicons-media-document',
      // 'show_in_admin_bar'     => true,
      // 'show_in_nav_menus'     => false,
      // 'can_export'            => true,
      // 'has_archive'           => $this->archive_page_slug,
      'has_archive'           => false,
      // 'exclude_from_search'   => false,
      // 'publicly_queryable'    => true,
      'rewrite'               => $rewrite,
      'capability_type'       => 'post',
      'show_in_rest'          => true,
    );

    // if ( current_theme_supports( 'custom_archive_' . $this->post_type ) ) {
    //   $args['has_archive'] = ( $this->archive_page && get_post( $this->archive_page ) ) ? urldecode( get_page_uri( $this->archive_page ) ) : $this->archive_page_slug;
    // } else {
    //   $args['has_archive'] = false;
    // }

    register_post_type( $this->post_type, $args );
  
  }


  public function create_taxonomies() {
      
    $cat1_labels = [
      'name' => __( 'Museum Types', 'nova' ),
      'singular_name' => __( 'Museum Type', 'nova' ),
      'menu_name' => __( 'Museum Types', 'nova' ),
      'all_items' => __( 'All Types', 'nova' ),
      'edit_item' => __( 'Edit Type', 'nova' ),
      'view_item' => __( 'View Type', 'nova' ),
      'update_item' => __( 'Update Type name', 'nova' ),
      'add_new_item' => __( 'Add new Type', 'nova' ),
      'new_item_name' => __( 'New Type name', 'nova' ),
      'parent_item' => __( 'Parent Type', 'nova' ),
      'parent_item_colon' => __( 'Parent Type:', 'nova' ),
      'search_items' => __( 'Search Types', 'nova' ),
      'popular_items' => __( 'Popular Types', 'nova' ),
      'separate_items_with_commas' => __( 'Separate Types with commas', 'nova' ),
      'add_or_remove_items' => __( 'Add or remove Types', 'nova' ),
      'choose_from_most_used' => __( 'Choose from the most used Types', 'nova' ),
      'not_found' => __( 'No Types found', 'nova' ),
      'no_terms' => __( 'No Types', 'nova' ),
      'items_list_navigation' => __( 'Types list navigation', 'nova' ),
      'items_list' => __( 'Types list', 'nova' ),
    ];
  
    $cat1_args = [
      'label' => __( 'Museum Types', 'nova' ),
      'labels' => $cat1_labels,
      'public' => true,
      'publicly_queryable' => false,
      'hierarchical' => false,
      'update_count_callback' => '_update_generic_term_count',
      //'show_ui' => true,
      //'show_in_menu' => true,
      'show_in_nav_menus' => false,
      'query_var' => sanitize_key( 'museum_type' ),
      //'rewrite' => false,
      'show_admin_column' => true,
      'show_in_rest' => true,
      //'rest_base' => 'museum_type',
      //'rest_controller_class' => 'WP_REST_Terms_Controller',
      //'show_in_quick_edit' => true,
      'admin_filter' => true,
      'sort' => true,
    ];
    register_taxonomy( 'museum_type', 'museum', $cat1_args );

    $cat2_labels = [
      'name' => __( 'Museum Localities', 'nova' ),
      'singular_name' => __( 'Museum Locality', 'nova' ),
      'menu_name' => __( 'Museum Localities', 'nova' ),
      'all_items' => __( 'All Localities', 'nova' ),
      'edit_item' => __( 'Edit Locality', 'nova' ),
      'view_item' => __( 'View Locality', 'nova' ),
      'update_item' => __( 'Update Locality name', 'nova' ),
      'add_new_item' => __( 'Add new Locality', 'nova' ),
      'new_item_name' => __( 'New Locality name', 'nova' ),
      'parent_item' => __( 'Parent Locality', 'nova' ),
      'parent_item_colon' => __( 'Parent Locality:', 'nova' ),
      'search_items' => __( 'Search Localities', 'nova' ),
      'popular_items' => __( 'Popular Localities', 'nova' ),
      'separate_items_with_commas' => __( 'Separate Localities with commas', 'nova' ),
      'add_or_remove_items' => __( 'Add or remove Localities', 'nova' ),
      'choose_from_most_used' => __( 'Choose from the most used Localities', 'nova' ),
      'not_found' => __( 'No Localities found', 'nova' ),
      'no_terms' => __( 'No Localities', 'nova' ),
      'items_list_navigation' => __( 'Localities list navigation', 'nova' ),
      'items_list' => __( 'Localities list', 'nova' ),
    ];
  
    $cat2_args = [
      'label' => __( 'Museum Localities', 'nova' ),
      'labels' => $cat2_labels,
      'public' => true,
      'publicly_queryable' => false,
      'hierarchical' => true,
      'update_count_callback' => '_update_generic_term_count',
      //'show_ui' => true,
      //'show_in_menu' => true,
      'show_in_nav_menus' => false,
      'query_var' => sanitize_key( 'museum_locality' ),
      //'rewrite' => false,
      'show_admin_column' => true,
      'show_in_rest' => true,
      //'rest_base' => 'museum_locality',
      //'rest_controller_class' => 'WP_REST_Terms_Controller',
      //'show_in_quick_edit' => true,
      // 'can_export'            => true,
      'admin_filter' => true,
      'sort' => true,
    ];
    register_taxonomy( 'museum_locality', 'museum', $cat2_args );
  
  }

  public function fix_thumbnail() {

    // if ( false == get_option( 'thumbnails_is_fixed' ) ) {

      $posts = new \WP_Query([
        'post_type' => 'museum',
        'posts_per_page' => -1
      ]);

      // echo '<pre style="margin-left: 300px">'; print_r( $posts ); echo '</pre>';

      foreach ( $posts->posts as $post ) {
        // $current_image = get_field( 'member_featured_image', $post->ID );
        $current_image = get_post_meta( $post->ID, 'member_featured_image', true );
        $fixed = get_post_meta( $post->ID, 'fixed_thumbnail', true );
        if ( ! $fixed ) {
          $success = update_post_meta( $post->ID, '_thumbnail_id', $current_image );

          if ( $success ) {
            add_post_meta( $post->ID, 'fixed_thumbnail', 1 );
          }
        }
        
      }


    // }
  }

  public function fix_gallery() {
    $posts = new \WP_Query([
      'post_type' => 'museum',
      'posts_per_page' => -1
    ]);

    foreach ( $posts->posts as $post ) {
      $fixed_gallery = get_post_meta( $post->ID, 'fixed_gallery', true );
      
      if ( ! $fixed_gallery ) {
        $current_gallery = get_post_meta( $post->ID, 'member_gallery', true );
        $values = str_replace( '[gallery ids="', '', $current_gallery );
        $values = str_replace( '"]', '', $values );
        $values = explode(',', $values);

        $success = update_post_meta( $post->ID, 'member_gallery_new', $values );

        if ( $success ) {
          add_post_meta( $post->ID, 'fixed_gallery', 1 );
        }
      }
    }
  }

  public function fix_content() {
    $posts = new \WP_Query([
      'post_type' => 'museum',
      'posts_per_page' => -1
    ]);

    foreach ( $posts->posts as $post ) {
      $fixed_gallery = get_post_meta( $post->ID, 'fixed_content', true );
      
      if ( ! $fixed_gallery ) {
        $current_content = get_post_meta( $post->ID, 'member_auto_content', true );
        $the_post = get_post( $post->ID, ARRAY_A );

        $the_post['post_content'] = $current_content;
        
        $success = wp_update_post( $the_post, true );

        if ( $success ) {
          add_post_meta( $post->ID, 'fixed_content', 1 );
        }
      }
    }
  }
}