<?php
/**
 * @package   Novastyle 2020
 * @author    Jon Täng <jon@bozzanova.se>
 * @link      http://www.bozzanova.se
 * @copyright 2020 Bozzanova AB
 */

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Users
 */
class Users {

  private static $instance = null;
	
	/**
	 * Instance function
	 *
	 * @return Theme
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	
	/**
	 * Constructor
	 */
	public function __construct() {
    add_action( 'init', [ $this, 'user_role_site_owner'] );
    add_action( 'init', [ $this, 'user_role_member'] );
    add_action( 'admin_init', [ $this, 'menu_pages'] );
    add_action( 'admin_menu', [ $this, 'clean_admin_menu' ], 999 );

    add_filter( 'custom_menu_order', '__return_true' );
    // add_filter( 'custom_menu_order', [ $this, 'custom_menu_order' ], 999, 1 );
    add_filter( 'menu_order', [ $this, 'custom_menu_order' ], 999, 1 );
    
    add_action( 'admin_bar_menu', [ $this, 'customize_toolbar' ], 999 );
    
    add_action( 'wp_dashboard_setup', [ $this, 'wporg_remove_all_dashboard_metaboxes' ] );

    // REMOVE COMMENTS
    add_action('admin_init', [ $this, 'remove_comments' ] );
    // Close comments on the front-end
    add_filter('comments_open', '__return_false', 20, 2);
    add_filter('pings_open', '__return_false', 20, 2);

    // Hide existing comments
    add_filter('comments_array', '__return_empty_array', 10, 2);

    // Remove comments page in menu
    add_action('admin_menu', [ $this, 'remove_comments_menu' ] );

    // Remove comments links from admin bar
    add_action('init', [ $this, 'remove_comments_adminbar' ]);

    // HIDE ADMIN FROM MEMBER
    // Custom Login
    add_filter( 'login_redirect', [ $this, 'login_redirect' ], 10, 3 );
    // Hide Admin
    add_action( 'wp_head', [ $this, 'hide_admin' ] );
    add_action( 'init', [ $this, 'admin_redirects' ] );
    add_action( 'wp', [ $this, 'redirect_private_page_to_login' ] );
  }
  
  /**
   * Create User Group Site Owner
   * Administrator role with some system capabilities removed
   */
  public function user_role_site_owner() {
    // Un comment next lines in DEV Mode
    update_option( 'role_site_owner_created', false );
    // remove_role( 'site_owner' );

    if ( ! get_option( 'role_site_owner_created' ) ) {

      $caps = get_role('administrator')->capabilities;

      // echo '<pre style="margin-left:300px">'; print_r( $caps ); echo '</pre>';

      $unwanted = [
        // Core
        'update_core' => 1,
        'delete_site' => 1,
        'unfiltered_html' => 1,
        // Plugin
        'activate_plugins' => 1,
        'delete_plugins' => 1,
        'install_plugins' => 1,
        'update_plugins' => 1,
        'edit_plugins' => 1,
        // Theme
        'switch_themes' => 1,
        'update_themes' => 1,
        'install_themes' => 1,
        'delete_themes' => 1,
        'edit_themes' => 1,
        // User
        'delete_users' => 1,
        'remove_users' => 1,
        'promote_users' => 1,
        // Misc
        'export' => 1,
        'import' => 1,
        'view_site_health_checks' => 1,
      ];

      $wanted = [];

      $caps = array_diff_key( $caps, $unwanted );
      $caps = array_merge( $caps, $wanted );
      
      add_role( 'site_owner', __('Site Owner', 'nova'), $caps );

      // update_option( 'role_site_owner_created', true );
    }
  }

  /**
   * Create User Role: Member
   */
  public function user_role_member() {
    // Un comment next lines in DEV Mode
    update_option( 'role_member_created', false );
    remove_role( 'member' );

    if ( ! get_option( 'role_member_created' ) ) {

      $caps = get_role('subscriber')->capabilities;

      // echo '<pre style="margin-left:300px">'; print_r( $caps ); echo '</pre>';

      $unwanted = [];
      $wanted = [
        'read' => 1,
        'read_private_pages' => 1,
      ];

      $caps = array_diff_key( $caps, $unwanted );
      $caps = array_merge( $caps, $wanted );
      
      add_role( 'member', __('Member', 'nova'), $caps );

      update_option( 'role_member_created', true );
    }
  }

  /**
   * Add Menu Pages
   */
  public function menu_pages() {
    add_menu_page( 
      __('Menus'),
      __('Menus'),
      'manage_options',
      'nav-menus.php',
      [],
      'dashicons-editor-justify',
      60
    );
  }

  /**
   * Clean up WP Admin Menu
   */
  public function clean_admin_menu() {
    $current_user = wp_get_current_user();
    // echo '<pre style="margin-left:300px">'; print_r( $current_user ); echo '</pre>';

    if( $current_user->has_cap( 'read' ) ) {
      // remove_menu_page( 'edit.php' ); // Posts
      //remove_menu_page( 'upload.php' ); // Media
      remove_menu_page( 'link-manager.php' ); // Links
      remove_menu_page( 'edit-comments.php' ); // Comments
      remove_menu_page( 'admin.php?page=rank-math' ); // Comments
      // remove_menu_page( 'tools.php' ); // Tools
      // remove_menu_page( 'edit.php?post_type=elementor_library' ); // Tools
    }

    if ( in_array( 'editor', $current_user->roles ) ) {
      remove_menu_page( 'tools.php' ); // Tools
      remove_menu_page( 'admin.php?page=rank-math-seo-analysis' ); // Comments
    }

    if ( in_array( 'site_owner', $current_user->roles ) ) {
      // remove_menu_page( 'edit.php' ); // Posts
      //remove_menu_page( 'upload.php' ); // Media
      remove_menu_page( 'link-manager.php' ); // Links
      remove_menu_page( 'edit-comments.php' ); // Comments
      //remove_menu_page( 'edit.php?post_type=page' ); // Pages
      //remove_menu_page( 'plugins.php' ); // Plugins
      remove_menu_page( 'themes.php' ); // Appearance
      //remove_menu_page( 'users.php' ); // Users
      //remove_menu_page( 'tools.php' ); // Tools
      remove_menu_page( 'options-general.php' ); // Settings
      
      remove_menu_page( 'edit.php?post_type=acf-field-group' ); // ACF
      
      remove_menu_page( 'backwpup' ); // Rank Math
      remove_menu_page( 'rank-math' ); // Rank Math
      remove_menu_page( 'admin.php?page=rank-math' ); // Rank Math
      remove_menu_page( 'admin.php?page=rank-math-status' ); // Rank Math
      // remove_menu_page( 'tools.php?page=action-scheduler' ); // Rank Math

      remove_submenu_page( 'themes.php', 'nav-menus.php');
      
    }
  }

  /**
   * Change order on WP Admin Menu
   * 
   * @param array $menu_order original menu order
   * @return array $menu_order returns new menu order
   */
  public function custom_menu_order( $menu_order ) {
    // echo '<pre style="margin-left:300px;">'; print_r( $menu_order ); echo '</pre>';
    
    if ( ! $menu_order ) {
      return true;
    }

    $menu_order = array(
      'index.php',                      // Dashboard
      // 'message-banner',
      'separator1',                     // First separator
      // 'nestedpages',
      'edit.php?post_type=page',        // Pages
      'edit.php',                       // Posts
      'edit.php?post_type=news',        // News
      'edit.php?post_type=museum',      // Museum
      'edit.php?post_type=place',       // Place
      'edit.php?post_type=event',       // Event
      'edit.php?post_type=activity',    // Activity
      'edit.php?post_type=faq',         // FAQ
      'edit.php?post_type=contact',     // Contact
      'edit.php?post_type=staff',       // Staff
      'edit.php?post_type=wod',         // Real Estate
      'upload.php',                     // Media
      'separator2',                     // Second separator
      'themes.php',                     // Appearance
      'nav-menus.php',
      'company-settings',
      'site-settings',
      'plugins.php',                    // Plugins
      'users.php',                      // Users
      'tools.php',                      // Tools
      'options-general.php',            // Settings
      'rank-math',
      'link-manager.php',               // Links
      'edit-comments.php',              // Comments
      'separator-last',                 // Last separator
    );
    // echo '<pre style="margin-left:300px;">'; print_r( $menu_order ); echo '</pre>';
    return $menu_order;
  }

  /**
   * Customize the WP Admin Toolbar
   * 
   * @param array $wp_admin_bar current item order
   * @return array $wp_admin_bar new item order
   */
  public function customize_toolbar( $wp_admin_bar ){
    $current_user = wp_get_current_user();

    $wp_admin_bar->remove_node( 'wp-logo' );
    $wp_admin_bar->remove_node( 'about' );
    $wp_admin_bar->remove_node( 'wporg' );
    $wp_admin_bar->remove_node( 'feedback' );
    $wp_admin_bar->remove_node( 'comments' );
    $wp_admin_bar->remove_node( 'documentation' );
    $wp_admin_bar->remove_node( 'support-forums' );
    $wp_admin_bar->remove_node( 'new-user' );
    // $wp_admin_bar->remove_node( 'top-secondary' );
    
    if ( in_array( 'site_owner', $current_user->roles ) ) {
      $wp_admin_bar->remove_node( 'updates' );
      $wp_admin_bar->remove_node( 'new-post' );
      $wp_admin_bar->remove_node( 'rank-math' );
      $wp_admin_bar->remove_node( 'query-monitor' );
      $wp_admin_bar->remove_node( 'query-monitor-placeholder' );
    }

  }
  
  /**
   * Remove Dashboard Widgets
   */
  function wporg_remove_all_dashboard_metaboxes() {
    $current_user = wp_get_current_user();
    if ( in_array( 'administrator', $current_user->roles ) ) {
      return;
    }
    // Remove Welcome panel
    remove_action( 'welcome_panel', 'wp_welcome_panel' );
    // Remove the rest of the dashboard widgets
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'health_check_status', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
    // Elementor
    remove_meta_box( 'e-dashboard-overview', 'dashboard', 'normal');
  }

  /**
   * Remove comments
   */
  public function remove_comments() {
    // Redirect any user trying to access comments page
    global $pagenow;
    
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
  }

  /**
   * Remove comments in admin menu
   */
  public function remove_comments_menu() {
    // Remove Comments in Menu
    remove_menu_page('edit-comments.php');
    // Remove Discussion Option Page
    remove_submenu_page('options-general.php', 'options-discussion.php');
  }

  /**
   * Remove comments in admin bar
   */
  public function remove_comments_adminbar() {
    remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
  }
  
  // HIDE ADMIN FROM USERS

  /**
   * WordPress function for redirecting users on login based on user role
   */
  public function login_redirect( $url, $request, $user ) {
    // Check User
    if ( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {

      // If Admin
      if ( $user->has_cap( 'administrator' ) ) {
          $url = admin_url();
      
      // If Member
      } elseif ( $user->has_cap( 'member' ) ) { 
        // If Start Page is set
        if ( $page = get_field( 'member_start_page', 'option' ) ) {
          $url = get_permalink( $page );

        } else {
          $url = home_url();

        }
      
      // If Other
      } else {
        $url = home_url();

      }
    }
    return $url;
  }
  /**
   * Hide Admin Bar and Alter Body Class
   */
  public function hide_admin() {
    $current_user = wp_get_current_user(); // Current User
    
    // If User is Subscriber or Member
    if ( current_user_can( 'subscriber' ) || $current_user->has_cap( 'member' )  ) {

      // Hide Admin Bar
      add_filter('show_admin_bar','__return_false');

      // Alter Body Class
      add_filter( 'body_class', function( array $classes ) {
        
        // Remove 'logged-in' class
        if ( in_array( 'logged-in', $classes ) ) {
          unset( $classes[ array_search( 'logged-in', $classes ) ] );
        }

        // Add 'member-logged-in' class
        $classes = array_merge( $classes, array( 'member-logged-in' ) );

        return $classes;

      } );
    }

  }

  /**
   * Redirect Members from Admin-pages
   */
  public function admin_redirects() {
    $admin_url = admin_url(); // Admin URL
    $current_url = home_url( $_SERVER['REQUEST_URI'] ); // Current Page URL
    $current_user = wp_get_current_user(); // Current User

    // Redirect If current URL contains Admin URL and current user is Subscriber/Member
    if ( str_contains( $current_url, $admin_url ) && ( current_user_can( 'subscriber' ) || $current_user->has_cap( 'member' ) ) ) {

      // Check if start page is set
      if ( $page = get_field( 'member_start_page', 'option' ) ) {
        $url = get_permalink( $page );

      } else {
        $url = home_url();

      }

      // Do redirect
      wp_redirect( $url );
      exit;

    }
  }

  /**
   * Redirect private pages to Login Screen
   */
  function redirect_private_page_to_login(){
    $queried_object = get_queried_object();

    if ( isset( $queried_object->post_status ) && 'private' === $queried_object->post_status && !is_user_logged_in() ) {
        
      //  Redirect to login page with redirect to same page
      wp_safe_redirect( wp_login_url( get_permalink( $queried_object->ID ) ) );
      exit;

    }

  }
  
}