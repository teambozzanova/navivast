<?php
/**
 * @package   Novastyle 2020
 * @author    Jon Täng <jon@bozzanova.se>
 * @link      http://www.bozzanova.se
 * @copyright 2020 Bozzanova AB
 */

namespace novatheme;

use novatheme\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Novastyle Elementor Setup
 */
class Elementor {

    private static $instance = null;

	public $theme_version = 1;

	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '5.4';

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @static
	 *
	 * @var Elementor_Test_Extension The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance function
	 *
	 * @return Elementor
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Class Construct
	 *
	 * @return void
	 */
	public function __construct() {

		//add_action( 'plugins_loaded', [ $this, 'init' ] );
		$this->init();

		// ACTIONS
		add_action( 'elementor/frontend/after_register_scripts', array( $this, 'register_frontend_scripts' ) );
		// add_action( 'elementor/theme/register_locations', [ $this, 'elementor_register_elementor_locations' ] );
	}

	/**
	 * Registers required JS files
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function register_frontend_scripts() {
		wp_register_script(
			'nova-map-widget',
			JS_URL. 'map-widget.js',
			array( 'jquery', 'nova-gmap' ),
			'',
			true
		);
	}
    /**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Elementor (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {
        // echo '<pre>'; print_r( 'ELEMENTOR LOADED' ); echo '</pre>';
		// Check if Elementor installed and activated
		// if ( ! did_action( 'elementor/loaded' ) ) {
		// 	add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
		// 	return;
		// }

		// Check for required Elementor version
		// if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
		// 	add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
		// 	return;
		// }

		// Check for required PHP version
		// if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
		// 	add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
		// 	return;
		// }

		
		// ADD ACTIONS
		add_action( 'elementor/elements/categories_registered', [ $this, 'add_widget_categories' ] );
		// add_action( 'elementor/editor/before_enqueue_styles', array( $this, 'enqueue_editor_styles' ) ); // Load Editor styles
		add_action( 'elementor/widgets/register', [ $this, 'init_widgets' ] ); // Load Widgets
		// add_action( 'elementor/editor/before_enqueue_scripts', array( $this, 'enqueue_editor_scripts' ) ); // Enqueue Editor scripts (google maps)
		// add_action( 'elementor/preview/enqueue_styles', array( $this, 'enqueue_preview_styles' ) ); // Enqueue Editor Preview Styles
		// add_action( 'elementor/frontend/after_register_styles', array( $this, 'register_frontend_styles' ) ); // Register Frontend Styles
		// add_action( 'elementor/frontend/after_register_scripts', array( $this, 'register_frontend_scripts' ) ); // Register Frontend Scripts
		//add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );

		add_action( 'elementor/editor/before_enqueue_scripts', [$this, 'scripts'], 99 );
		// add_action( 'elementor/editor/after_enqueue_scripts', array( $this, 'after_enqueue_scripts' ) ); // Loads Editor Scripts for controls
		// add_action( 'elementor/editor/before_enqueue_scripts', array( $this, 'enqueue_editor_cp_scripts' ), 99 );
	}

    public function scripts() {
		// if ( $gmap_key = get_option( 'options_google_maps_api_key' ) ) {
		// 	//https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY
		// 	wp_register_script( 'nova-gmap',  'https://maps.googleapis.com/maps/api/js?key=' . $gmap_key, [ 'elementor-editor' ], '1.0.0', true );
		// 	wp_register_script( 'nova-map',  ASSET_URL . 'js/map-widget.js', [ 'nova-gmap' ], '1.0.0', true );

		// 	wp_register_style( 'nova-map', ASSET_URL. 'css/map-widget.css' );
		// 	//add_action( 'wp_footer', [ $this, 'render_posts' ] );
		// }
	}

	public function add_widget_categories( $elements_manager ) {
		$elements_manager->add_category(
			'nova_elements',
			[
				'title' => __( 'Theme', 'nova' ),
				'icon' => 'fa fa-plug',
			],
			1
		);
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'nova' ),
			'<strong>' . esc_html__( 'Elementor Test Extension', 'nova' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'nova' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'nova' ),
			'<strong>' . esc_html__( '', 'nova' ) . '</strong>',
			'<strong>' . esc_html__( '', 'nova' ) . '</strong>',
				self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'nova' ),
			'<strong>' . esc_html__( 'Elementor Test Extension', 'nova' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'nova' ) . '</strong>',
				self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
	
	}

	/**
	 * Init Widgets
	 *
	 * Include widgets files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_widgets() {

		// Include Widget files
		// require_once( WIDGET_PATH . 'events-widget.php' );
		// require_once( WIDGET_PATH . 'media-widget.php' );
		// require_once( WIDGET_PATH . 'timeline-widget.php' );
		// require_once( WIDGET_PATH . 'pause-embed-widget.php' );
		// require_once( WIDGET_PATH . 'real-estate-widget.php' );

		// Register widget
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Event_List_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Event_Details_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Facilities_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Info_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Map_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\POI_Filter() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\POI_List() );
		// \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Navigation_Widget() );
		

	}

	

	function elementor_register_elementor_locations( $elementor_theme_manager ) {
		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_elementor_locations', [ true ], '2.0', 'hello_elementor_register_elementor_locations' );
		if ( apply_filters( 'hello_elementor_register_elementor_locations', $hook_result ) ) {
			$elementor_theme_manager->register_all_core_location();
		}
	}
}