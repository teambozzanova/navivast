<?php
/**
 * Functions and definitions
 *
 * @package NAViVäst
 */

namespace novatheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Theme
define( 'NAME', 'NovaStyle' );
define( 'VERSION', '2022' );
define( 'THEME_URL', get_template_directory_uri() . '/' );
define( 'THEME_PATH', get_template_directory() . '/' );
// Assets
define( 'ASSET_URL', THEME_URL . 'assets/' );
define( 'ASSET_PATH', THEME_PATH . 'assets/' );
define( 'CSS_URL', ASSET_URL . 'css/' );
define( 'CSS_PATH', ASSET_PATH . 'css' );
define( 'FONT_URL', ASSET_URL . 'fonts/' );
define( 'FONT_PATH', ASSET_PATH . 'fonts/' );
define( 'IMG_URL', ASSET_URL . 'images/' );
define( 'IMG_PATH', ASSET_PATH . 'images/' );
define( 'JS_URL', ASSET_URL . 'js/' );
define( 'JS_PATH', ASSET_PATH . 'js/' );
// Framework files
define( 'CLS_URL', THEME_URL . 'classes/' );
define( 'CLS_PATH', THEME_PATH . 'classes/' );
define( 'INC_URL', THEME_URL . 'includes/' );
define( 'INC_PATH', THEME_PATH . 'includes/' );
define( 'WIDGET_URL', THEME_URL . 'elementor/' );
define( 'WIDGET_PATH', THEME_PATH . 'elementor/' );
// Language
define( 'LANG_URL', THEME_URL . 'languages/' );
define( 'LANG_PATH', THEME_PATH . 'languages/' );
// Template files
define( 'PART_URL', THEME_URL . 'parts/' );
define( 'PART_PATH', THEME_PATH . 'parts/' );
define( 'PART', 'parts/' );

require_once 'autoloader.php';

/**
 * Initialize plugin
 *
 * @return $theme
 */
function load_theme() {
	$autoloader = new Autoloader();
	
    $autoloader->register();
	$autoloader->addNamespace( 'novatheme', __DIR__ . '/includes' );
	// $autoloader->addNamespace( 'novatheme\Widgets', __DIR__ . '/widgets' );

	$instance = Theme::instance( __FILE__, '1.0.0' );

	return $instance;
}

load_theme();

// echo '<pre>'; print_r( $theme ); echo '</pre>';